﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using DAL.Classes;


namespace Engine.Controllers
{
    public class AdminCityController : Controller
    {
        AdminCity sample = new AdminCity();

        public ActionResult Create()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            List<SelectListItem> list = new List<SelectListItem>();
            string[] aar = { "Punjab", "Sindh", "Khyber Pakhtunkhwa", "Islamabad Capital Territory", "Gilgit-Baltistan", "Federally Administered Tribal Areas", "Balochistan", "Azad Jammu & Kashmir" };
            foreach (var a in aar)
            {
                list.Add(new SelectListItem { Text = a, Value = a, Selected = false });
            }
            ViewBag.Province = list;
            return View();
        }

        [HttpPost]
        public ActionResult AddCity(City obj)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Add(obj);
            return RedirectToAction("ViewAll", "AdminCity");
        }

        public ActionResult Details(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            var obj = sample.GetDetails(id);
            List<SelectListItem> list = new List<SelectListItem>();
            string[] aar = { "Punjab", "Sindh", "Khyber Pakhtunkhwa", "Islamabad Capital Territory", "Gilgit-Baltistan", "Federally Administered Tribal Areas", "Balochistan", "Azad Jammu & Kashmir" };
            foreach (var a in aar)
            {
                if(a==obj.Province)
                {
                    list.Add(new SelectListItem { Text = a, Value = a, Selected = true});
                }
                else
                {
                    list.Add(new SelectListItem { Text = a, Value = a, Selected = false });
                }
            }
            ViewBag.Province = list;
            return View(obj);
        }

        [HttpPost]
        public ActionResult EditCity(City obj)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Update(obj);
            return RedirectToAction("ViewAll", "AdminCity");
        }

        public ActionResult ViewAll()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View(sample.GetAll());
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll", "AdminCity");
        }

    }
}