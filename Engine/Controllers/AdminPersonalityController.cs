﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using DAL.Classes;

namespace Engine.Controllers
{
    public class AdminPersonalityController : Controller
    {
        AdminPersonality sample = new AdminPersonality();

        public ActionResult Create()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View();
        }

        [HttpPost]
        public ActionResult AddPersonality(PersonalityType obj)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Add(obj);
            return RedirectToAction("ViewAll", "AdminPersonality");
        }
       
        public ActionResult Details(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View(sample.GetDetails(id));
        }

        [HttpPost]
        public ActionResult EditPersonality(PersonalityType obj)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
                sample.Update(obj);
                return RedirectToAction("ViewAll", "AdminPersonality");
        }

        public ActionResult ViewAll()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View(sample.GetAll());
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll", "AdminPersonality");
        }
    }
}