﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Classes;
using DAL.Models;

namespace Engine.Controllers
{
    public class CityController : ApiController
    {
        public object GetCities()
        {
            List<City>list =  new UserCity().GetAllCities();
            var ob = list.Select(x => new
            {
                x.Id,
                x.Name
            });
            return ob;
        }
    }
}
