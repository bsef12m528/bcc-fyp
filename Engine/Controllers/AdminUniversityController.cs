﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using DAL.Classes;

namespace Views.Controllers
{
    public class AdminUniversityController : Controller
    {
        // GET: AdminUniversity
        AdminUniversity sample = new AdminUniversity();

        public ActionResult Create()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            ViewBag.CityId = new SelectList(sample.GetCities(), "Id", "Name");
            ViewBag.CategoryId = new SelectList(sample.GetCategories(), "Id", "Name");
            return View();
        }

        [HttpPost]
        public JsonResult CreateUniversity(University obj, UniversityCampuss [] arr)
        {
            obj.Id=sample.Add(obj);
            sample.AddCampuses(obj.Id, arr);
            return Json("Success");
        }

        public ActionResult Details(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            University obj = sample.GetDetails(id);
            ViewBag.CityId = new SelectList(sample.GetCities(), "Id", "Name");
            ViewBag.CategoryId = new SelectList(sample.GetCategories(), "Id", "Name",obj.CategoryId);
            List<SelectListItem> list = new List<SelectListItem>();
            if(obj.Sector=="Government")
            {
                list.Add(new SelectListItem {Text="Government",Value="Government",Selected=true });
                list.Add(new SelectListItem { Text = "Private", Value = "Private", Selected = false });
            }
            else
            {
                list.Add(new SelectListItem { Text = "Government", Value = "Government", Selected = false });
                list.Add(new SelectListItem { Text = "Private", Value = "Private", Selected = true });
            }
            ViewBag.Sector = list;
            return View(obj);
        }

        [HttpPost]
        public JsonResult EditUniversity(University obj, UniversityCampuss [] arr)
        {
            sample.Update(obj, arr);
            return Json("Success");
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll");
        }

        public ActionResult ViewAll()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            List<University> list = sample.GetAll();
            return View(list);
        }
    }
}