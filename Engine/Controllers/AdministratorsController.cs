﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using DAL.Classes;

namespace Engine.Controllers
{
    public class AdministratorsController : Controller
    {
        private AdminAuthentication sample = new AdminAuthentication();
        private DbEntities db = new DbEntities();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Administrator obj)
        {
            Administrator tmp = sample.GetDetails(obj);
            if(tmp==null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            else
            {
                tmp.Password = DecodeFrom64(tmp.Password);
                if(tmp.Password!=obj.Password)
                {
                    return RedirectToAction("Index", "Administrators");
                }
            }
            Session["Id"] =tmp.Id;
            Session["Role"] = tmp.Role;
            Session["Username"] = tmp.Username;
            return RedirectToAction("ViewAll","AdminUniversity");
        }

        [HttpPost]
        public JsonResult CheckUsername(string username)
        {
            Administrator tmp = new Administrator();
            tmp.Username = username;
            tmp = sample.GetDetails(tmp);
            if(tmp==null)
            {
                return Json("true");
            }
            return Json("false");
        }

        [HttpPost]
        public ActionResult AddAdmin(Administrator obj)
        {
            if (Session["Id"] == null || Session["Role"].ToString()=="contributer")
            {
                return RedirectToAction("Index", "Administrators");
            }
            obj.Role = "contributer";
            obj.Password = EncodePasswordToBase64(obj.Password);
            sample.Add(obj);
            return RedirectToAction("ViewAll");
        }

        public ActionResult Logout()
        {
            Session["Role"] = null;
            Session["Id"] = null;
            return RedirectToAction("Index","Administrators");
        }
        public ActionResult ViewAll()
        {
            if (Session["Id"] == null || Session["Role"].ToString() == "contributer")
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View(sample.GetAll());
        }
       
        public ActionResult Create()
        {
            if (Session["Id"] == null || Session["Role"].ToString() == "contributer")
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null || Session["Role"].ToString() == "contributer")
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll","Administrators");
        }

        public ActionResult ChangePassword()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View();
        }

        [HttpPost]
        public ActionResult EditAdmin(Administrator obj)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            obj.Id = Convert.ToInt32(Session["Id"]);
            obj.Role= Session["Role"].ToString();
            obj.Password = EncodePasswordToBase64(obj.Password);
            sample.Update(obj);
            return RedirectToAction("Logout", "Administrators");
        }

        public static string EncodePasswordToBase64(string password)
        {
            try
            {
                byte[] encData_byte = new byte[password.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in base64Encode" + ex.Message);
            }
        } 

        //this function Convert to Decord your Password
        public string DecodeFrom64(string encodedData)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encodedData);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }
    }
}
