﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using DAL.Classes;

namespace Views.Controllers
{
    public class AdminCareerController : Controller
    {
        AdminCareer sample = new AdminCareer();
        DbEntities db = new DbEntities();

        public ActionResult Create()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            ViewBag.PersonalityTypeId = new SelectList(sample.GetPersonalityTypes(), "Id", "Name");
            ViewBag.DegreeId = new SelectList(sample.GetDegrees(), "Id", "DegreeName");
            return View();
        }

        [HttpPost]
        public JsonResult CreateCareer(Career obj, int [] arr)
        {
                int id = sample.Add(obj);
                sample.AddDegrees(id, arr);
                return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateCareer(Career obj, int[] arr)
        {
            sample.Update(obj);
            sample.DeleteCareerDegrees(obj.Id);
            sample.AddDegrees(obj.Id, arr);
            return this.Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Details(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            Career obj = sample.GetDetails(id);
            ViewBag.PersonalityTypeId = new SelectList(sample.GetPersonalityTypes(), "Id", "Name",obj.PersonalityTypeId);
            List<Degree>list = sample.GetDegrees().ToList();
            List<DegreeCareer> list1 = obj.DegreeCareers.ToList();
            foreach(var a in list1)
            {
                list.Remove(a.Degree);
            }
            ViewBag.DegreeId = list;
            ViewBag.CurrentDegreeId = list1;
            return View(obj);
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll");
        }

        public ActionResult ViewAll()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            List<Career> list=sample.GetAll();
            return View(list);
        }
    }
}
