﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using System.Data;
using DAL.Classes;

namespace Views.Controllers
{
    public class AdminNewsFeedController : Controller
    {
        AdminNewsFeed sample = new AdminNewsFeed();

        public ActionResult Create()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            ViewBag.UniversityId = new SelectList(sample.GetUniversities(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UniversityNewss obj)
        {
            if(ModelState.IsValid)
            {
                sample.Add(obj);
                return RedirectToAction("ViewAll","AdminNewsFeed");
            }
            ViewBag.UniversityId = new SelectList(sample.GetUniversities(), "Id", "Name");
            return View();
        }

        public ActionResult Details(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            UniversityNewss obj= sample.GetDetails(id);
            ViewBag.UniversityId = new SelectList(sample.GetUniversities(), "Id", "Name", obj.UniversityId);
            return View(obj);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(UniversityNewss obj)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            if(ModelState.IsValid)
            {
                sample.Update(obj);
                return RedirectToAction("ViewAll", "AdminNewsFeed");
            }
            return View();
        }

        public ActionResult ViewAll()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            List<UniversityNewss> list = sample.GetAll();
            return View(list);
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll","AdminNewsFeed");
        }
    }
}