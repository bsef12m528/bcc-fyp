﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Models;
using DAL.Classes;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Engine.Controllers
{
    public class QuestionController : ApiController
    {
        public object GetQuestions()
        {
            UserQuestion obj = new UserQuestion();
            var x = obj.GetQuestions();

            var y = from z in x
                    select new
                        {
                            z.Id,
                            z.Statement
                        };

            return y;
        }

        [HttpPost]
        public object GetResult(List<AQuestion> json)
        {
            //json.ToString();
            UserQuestion uq = new UserQuestion();
            var x = uq.GetResult(json);

            return x;
        }
    }
}
