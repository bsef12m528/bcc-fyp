﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using DAL.Classes;
using System.IO;
using System.Configuration;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;

namespace Views.Controllers
{
    public class AdminCampusController : Controller
    {
        // GET: AdminCampus
        AdminCampus sample = new AdminCampus();
        static int campusid;

        public ActionResult Create()
        {
            if(Session["Id"]==null)
            {
                return RedirectToAction("Index","Administrators");
            }

            ViewBag.CityId = new SelectList(sample.GetCities(),"Id", "Name");
            ViewBag.UniversityId = new SelectList(sample.GetUniversities(), "Id", "Name");
            List<Degree> list = sample.GetDegrees().ToList();
            List<SelectListItem> DegreeSelect = new List<SelectListItem>();
            if(list!=null)
            {
                foreach (var a in list)
                {
                    DegreeSelect.Add(new SelectListItem { Text = a.DegreeName+"("+a.DegreeDuration+" Years)", Value = a.Id.ToString(), Selected = false });
                }
            }
            ViewBag.DegreeId = DegreeSelect;
            return View();
        }

        [HttpPost]
        public JsonResult AddCampus(UniversityCampuss obj, CampusDegree [] arr)
        {
            sample.Add(obj, arr);
            return Json("success");
        }

        public ActionResult Gallery(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            UniversityCampuss obj = sample.GetDetails(id);
            campusid = obj.Id;

            string connString = ConfigurationManager.ConnectionStrings["AzureStorageAccount"].ConnectionString;
            string destContainer = ConfigurationManager.AppSettings["destContainer"];

            //Get a reference to the storage account
            CloudStorageAccount sa = CloudStorageAccount.Parse(connString);
            CloudBlobClient bc = sa.CreateCloudBlobClient();

            // Get a reference to the container (creating it if necessary)
            CloudBlobContainer container = bc.GetContainerReference(destContainer);

            string pref = obj.Id + "_";

            var query = container.ListBlobs(prefix: pref);

            var fileName = "";

            List<string> images = new List<string>();
            foreach (var item in query)
            {
                fileName = item.Uri + "";
                fileName = fileName.Replace("https://mustqbilstorage.blob.core.windows.net/campuses/", "");
                images.Add(fileName);
            }
            ViewBag.Images = images;
            return View(obj);
            
        }


        [HttpPost]
        public JsonResult AddImage()
        {
            HttpPostedFileBase file = Request.Files[0];

            string connString = ConfigurationManager.ConnectionStrings["AzureStorageAccount"].ConnectionString;
            string destContainer = ConfigurationManager.AppSettings["destContainer"];

            //Get a reference to the storage account
            CloudStorageAccount sa = CloudStorageAccount.Parse(connString);
            CloudBlobClient bc = sa.CreateCloudBlobClient();

            // Get a reference to the container (creating it if necessary)
            CloudBlobContainer container = bc.GetContainerReference(destContainer);
            
            bool flag = true;
            int i = 1;
            while (flag == true)
            {
                string key = campusid + "_" + i + ".jpg";
                CloudBlockBlob b = container.GetBlockBlobReference(key);
                if (b.Exists())
                {
                    i++;
                }
                else
                {
                    b.UploadFromStream(file.InputStream);
                    flag = false;
                    return Json(key);
                }
            }
            return Json("Success");
        }




        #region checking if image exists at container
        public string CheckImageExistance()
        {
            string connString = ConfigurationManager.ConnectionStrings["AzureStorageAccount"].ConnectionString;
            string destContainer = ConfigurationManager.AppSettings["destContainer"];

            //Get a reference to the storage account
            CloudStorageAccount sa = CloudStorageAccount.Parse(connString);
            CloudBlobClient bc = sa.CreateCloudBlobClient();

            // Get a reference to the container (creating it if necessary)
            CloudBlobContainer container = bc.GetContainerReference(destContainer);


            if (container.GetBlockBlobReference("x.jpg").Exists())
            {
                return "blob exists";
            }
            else
                return "does not exists";

            //try
            //{
            //    //Fetches attributes of container
            //    container.FetchAttributes();
                
            //    return "Container exists..";
            //}
            //catch (Exception exc)
            //{
               
            //        // other exceptions
            //        return "Errr..: " + exc.Message;
            //}

        }
        #endregion


        [HttpPost]
        public JsonResult DeleteImage(string file)
        {

            string connString = ConfigurationManager.ConnectionStrings["AzureStorageAccount"].ConnectionString;
            string destContainer = ConfigurationManager.AppSettings["destContainer"];

            //Get a reference to the storage account
            CloudStorageAccount sa = CloudStorageAccount.Parse(connString);
            CloudBlobClient bc = sa.CreateCloudBlobClient();

            // Get a reference to the container (creating it if necessary)
            CloudBlobContainer container = bc.GetContainerReference(destContainer);
            CloudBlockBlob b = container.GetBlockBlobReference(file);

            if (b.Exists())
            {
                b.Delete();
                return Json("Success");
            }
            else
                return Json("error");
        }


        public ActionResult ViewAll()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            List<UniversityCampuss> list = sample.GetAll();
            return View(list);
        }

        public ActionResult Details(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            UniversityCampuss obj = sample.GetDetails(id);
            ViewBag.CityId = new SelectList(sample.GetCities(), "Id", "Name",obj.CityId);
            ViewBag.UniversityId = new SelectList(sample.GetUniversities(), "Id", "Name",obj.UniversityId);

            List<Degree> list = sample.GetDegrees().ToList();
            List<CampusDegree> exists = obj.CampusDegrees.ToList();
            List<SelectListItem> DegreeSelect = new List<SelectListItem>();
            if (list != null)
            {
                foreach (var a in list)
                {
                    dynamic tmp = exists.Where(x => x.UniversityCampusId == obj.Id && x.DegreeId == a.Id).Select(x => x).Count();
                    if(tmp==1)
                    {
                        DegreeSelect.Add(new SelectListItem { Text = a.DegreeName + "(" + a.DegreeDuration + " Years)", Value = a.Id.ToString(), Selected = true });
                    }
                    else
                    {
                        DegreeSelect.Add(new SelectListItem { Text = a.DegreeName + "(" + a.DegreeDuration + " Years)", Value = a.Id.ToString(), Selected = false });
                    }
               }
            }
            ViewBag.DegId = DegreeSelect;
            return View(obj);
        }

        [HttpPost]
        public JsonResult EditCampus(UniversityCampuss obj, CampusDegree [] arr)
        {
            sample.Update(obj, arr);
            return Json("success");
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll", "AdminCampus");
        }
    }
}