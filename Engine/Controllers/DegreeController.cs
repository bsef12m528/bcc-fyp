﻿using DAL.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Models;

namespace Engine.Controllers
{
    public class DegreeController : ApiController
    {
        public object GetDegree(int id)
        {
            UserDegree ud = new UserDegree();
            Degree d = ud.GetDegreeDetails(id);

            return new
            {
                d.Id,
                d.ShortName,
                d.DegreeDuration,
                d.DegreeName,
                CampusDegrees = d.CampusDegrees.Select(x => new
                {
                    x.DegreeId,
                    x.DegreeSystem,
                    x.UniversityCampusId,
                    UniversityCampuss = new
                    {
                        x.UniversityCampuss.CampusTitle,
                        x.UniversityCampuss.Address,
                        x.UniversityCampuss.UniversityId,
                        x.UniversityCampuss.AlternateName,
                        x.UniversityCampuss.CityId,
                        x.UniversityCampuss.Id,
                        City = new
                        {
                            x.UniversityCampuss.City.Id,
                            x.UniversityCampuss.City.Name,
                            x.UniversityCampuss.City.Province
                        },

                        University = new
                        {
                            x.UniversityCampuss.University.Id,
                            x.UniversityCampuss.University.Sector,
                            x.UniversityCampuss.University.Name,
                            x.UniversityCampuss.University.Description,
                            x.UniversityCampuss.University.Priority,
                            x.UniversityCampuss.University.AlternateName,
                            x.UniversityCampuss.University.WebAddress,
                            Category = new
                            {
                                Id = x.UniversityCampuss.University.Category.Id,
                                Name = x.UniversityCampuss.University.Category.Name,
                                Description = x.UniversityCampuss.University.Category.Description
                            }
                        }
                    }
                })
            };


        }


        public object GetAllDegrees()
        {
            UserDegree ud = new UserDegree();
            var x = ud.GetAll().Select(y => new
            {
                y.Id,
                y.DegreeName,
                y.ShortName,
                y.DegreeDuration
            });

            return x;

        }


        //[HttpGet]
        //public bool DoMywork()
        //{
        //    return new UserDegree().TrimAndRemoveSpace();
        //}

    }
}
