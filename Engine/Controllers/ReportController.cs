﻿using DAL.Classes;
using DAL.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Engine.Controllers
{
    public class ReportController : ApiController
    {
        public object SaveReport(JObject json)
        {
            JsonSerializer serializer = new JsonSerializer();
            UserTestReport us = (UserTestReport)serializer.Deserialize(new JTokenReader(json), typeof(UserTestReport));
            return new UserReport().SaveReport(us);
        }

        public object GetReport(int id)
        {
            UserTestReport utr = new UserReport().GetReport(id);
            return new
            {
                utr.Id,
                utr.ReportSummary,
                utr.Date,
                utr.UserId,
                RecommendedCareers = utr.RecommendedCareers.Select(x => new
                {
                    x.UserTestReportId,
                    x.Id,
                    x.CareerId,
                    Career = new
                    { 
                        x.Career.CareerName,
                        x.Career.PersonalityTypeId,
                        x.Career.Id,
                        x.Career.Details 
                    }
                })

            };
        }


        public object GetReportOfUser(int id)       //
        {
            List<UserTestReport> list = new UserReport().GetUserReport(id);

            var query = from utr in list
                        select new
                            {
                                utr.Id,
                                utr.ReportSummary,
                                utr.Date,
                                utr.UserId,
                                RecommendedCareers = utr.RecommendedCareers.Select(x => new
                                {
                                    x.UserTestReportId,
                                    x.Id,
                                    x.CareerId,
                                    Career = new
                                    {
                                        x.Career.CareerName,
                                        x.Career.PersonalityTypeId,
                                        x.Career.Id,
                                        x.Career.Details
                                    }
                                })
                            };

            return query;
        }
    }
}
