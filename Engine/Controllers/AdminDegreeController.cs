﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using DAL.Classes;

namespace Views.Controllers
{
    public class AdminDegreeController : Controller
    {
        AdminDegree sample = new AdminDegree();

        public ActionResult Create()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            ViewBag.CareerId = new SelectList(sample.GetCareers(), "Id", "CareerName");
            List<SelectListItem> list = new List<SelectListItem>();

            string [] str = {"4","5","3","2","1"};
            foreach(var a in str)
            {
                list.Add(new SelectListItem {Text=a+" Years",Value=a, Selected=false});
            }
            ViewBag.DegreeDuration = list;

            return View();
        }

        [HttpPost]
        public JsonResult CreateDegree(Degree obj, int [] arr)
        {
            int id=sample.Add(obj);
            sample.AddCareers(id, arr);
            return Json("Successfull");
        }
        
        public ActionResult ViewAll()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View(sample.GetAll());
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll", "AdminDegree");
        }

        public ActionResult Details(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            Degree obj = sample.GetDetails(id);
            List<DegreeCareer> list = obj.DegreeCareers.ToList();
            List<Career> list2 = sample.GetCareers().ToList();
            foreach(var a in list)
            {
                Career tmp = list2.First(x => x.Id == a.CareerId);
                list2.Remove(tmp);
            }
            ViewBag.CareerId = list2;
            ViewBag.CurrentCareerId = list;

            List<SelectListItem> l = new List<SelectListItem>();
            string[] str = { "1", "2", "3", "4", "5" };
            foreach (var a in str)
            {
                if(obj.DegreeDuration==a)
                {
                    l.Add(new SelectListItem { Text = a + " Years", Value = a, Selected = true});
                }
                else
                {
                    l.Add(new SelectListItem { Text = a + " Years", Value = a, Selected = false });
                }
                
            }
            ViewBag.DegreeDuration = l;
            return View(obj);
        }

        [HttpPost]
        public JsonResult UpdateDegree(Degree obj, int [] arr)
        {
            sample.Update(obj,arr);
            return Json("Successfull");
        }


        
    }


}