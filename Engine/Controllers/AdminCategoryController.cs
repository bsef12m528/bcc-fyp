﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Classes;
using DAL.Models;

namespace Engine.Controllers
{
    public class AdminCategoryController : Controller
    {
        // GET: AdminCategory
        AdminCategory sample = new AdminCategory();

        public ActionResult Create()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View();
        }

        [HttpPost]
        public ActionResult AddCategory(Category obj)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Add(obj);
            return RedirectToAction("ViewAll", "AdminCategory");
        }

        public ActionResult Details(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View(sample.GetDetails(id));
        }

        [HttpPost]
        public ActionResult EditCategory(Category obj)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Update(obj);
            return RedirectToAction("ViewAll", "AdminCategory");
        }

        public ActionResult ViewAll()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View(sample.GetAll());
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll", "AdminCategory");
        }
    }
}