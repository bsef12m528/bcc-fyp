﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Models;
using DAL.Classes;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Engine.Controllers
{
    public class UserController : ApiController
    {
        [HttpPost]
        public bool GetSignup(JObject json)
        {
            JsonSerializer serializer = new JsonSerializer();
            User us = (User)serializer.Deserialize(new JTokenReader(json), typeof(User));


            //User user = JsonConvert.DeserializeObject<User>(jsonString);
            UserDal obj = new UserDal();
            return obj.Signup(us);
        }

        [HttpPost]
        public User GetSignin(JObject json)
        {
            JsonSerializer serializer = new JsonSerializer();
            User us = (User)serializer.Deserialize(new JTokenReader(json), typeof(User));
            UserDal obj = new UserDal();
            return obj.Signin(us);
        }


        [HttpPost]
        public string UpdateUser(JObject json)
        {
            JsonSerializer serializer = new JsonSerializer();
            User us = (User)serializer.Deserialize(new JTokenReader(json), typeof(User));
            UserDal obj = new UserDal();
            return obj.UpdateUser(us);
        }

        public string GetConString()
        {
            return new UserDal().GetConString();
        }
    }
}