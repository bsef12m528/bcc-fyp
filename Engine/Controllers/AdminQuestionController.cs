﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Models;
using DAL.Classes;

namespace Engine.Controllers
{
    public class AdminQuestionController : Controller
    {
        // GET: AdminQuestion
        AdminQuestion sample = new AdminQuestion();

        public ActionResult Create()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            ViewBag.Career = new SelectList(sample.GetCareer(),"Id","CareerName");
            return View();
        }

        [HttpPost]
        public JsonResult CreateQuestion(string s, int [] arr)
        {
            Question obj = new Question();
            obj.Statement = s;
            int id = sample.Add(obj);

            sample.AddQuestionCareer(id, arr);

            return Json("Successfull");
        }

        public ActionResult ViewAll()
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            return View(sample.GetAll());
        }

        [HttpPost]
        public JsonResult Update(Question obj, int [] arr)
        {
            sample.Update(obj,arr);
            return Json("Successfull");
        }

        public ActionResult Delete(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            sample.Delete(id);
            return RedirectToAction("ViewAll");
        }

        public ActionResult Details(int id)
        {
            if (Session["Id"] == null)
            {
                return RedirectToAction("Index", "Administrators");
            }
            Question obj = sample.GetDetails(id);
            List<QuestionCareer> list = obj.QuestionCareers.ToList();
            List<Career> list2 = sample.GetCareer().ToList();
            if(list!=null)
            {
                foreach (var a in list)
                {
                    Career tmp = new Career();
                    try
                    {
                        tmp = list2.First(x => x.Id == a.CareerId);
                    }
                    catch (Exception e) { }
                    list2.Remove(tmp);
                }
            }
           
            List<SelectListItem> career = new List<SelectListItem>();
            if(list!=null)
            {
                foreach (var a in list)
                {
                    career.Add(new SelectListItem { Text = a.Career.CareerName, Value = a.Career.Id.ToString(), Selected = true });
                }
            }
            if(list2!=null)
            {
                foreach (var a in list2)
                {
                    career.Add(new SelectListItem { Text = a.CareerName, Value = a.Id.ToString(), Selected = false });
                }
            }
            ViewBag.Career = career;
            return View(obj);
        }
    }
}