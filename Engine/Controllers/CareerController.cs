﻿using DAL.Classes;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Engine.Controllers
{
    public class CareerController : ApiController
    {
        public object GetCareer(int id)
        {

            Career c =  new UserCareer().GetCareer(id);
            return new
            {
                c.Id,
                c.CareerName,
                DegreeCareers = c.DegreeCareers.Select(x => new 
                {
                    x.CareerId,
                    x.DegreeId, 
                    x.Id,
                    Degree = new 
                    { 
                        x.Degree.DegreeName,
                        x.Degree.ShortName,
                        x.Degree.Id,
                        x.Degree.DegreeDuration
                    }
                })
            };

        }


    }
}
