﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DAL.Models;
using DAL.Classes;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Web;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Engine.Models;
//using System.Linq.Dynamic;

namespace Engine.Controllers
{
    public class UniversityController : ApiController
    {
        public object GetUniversities()
        {
            UserUniversity obj = new UserUniversity();
            //return obj.GetUniversities();

            var x = obj.GetUniversities();
            var yy = from z in x
                     select new
                     {
                         z.Id,
                         z.Sector,
                         z.Name,
                         z.Description,
                         z.Priority,
                         z.AlternateName,
                         z.WebAddress,
                         Category = new 
                         {
                             Id =  z.Category.Id,
                             Name = z.Category.Name,
                             Description = z.Category.Description
                         }

                     };


            return yy;

        }


        //for accessing universites according to sector
        public object GetUniversitiesBySector(string json)
        {
            UserUniversity obj = new UserUniversity();
            var x = obj.GetUniversitiesBySector(json);

            var yy = from z in x
                     select new
                     {
                         z.Id,
                         z.Sector,
                         z.Name,
                         z.Description,
                         z.Priority,
                         z.AlternateName,
                         z.WebAddress,
                         Category = new
                         {
                             Id = z.Category.Id,
                             Name = z.Category.Name,
                             Description = z.Category.Description
                         }

                     };


            return yy;

        }



        //return campuses that are in specific cities & for specific degrees
        [HttpPost]
        public object GetCampusesInCities(JObject jobject)
        {
            JsonSerializer serializer = new JsonSerializer();
            UniversityFilter ust = (UniversityFilter)serializer.Deserialize(new JTokenReader(jobject), typeof(UniversityFilter));

            List<int> json = ust.CityIds;
            List<int> deg = ust.DegreeIds;
            UserUniversity obj = new UserUniversity();
            List<UniversityCampuss> list = new List<UniversityCampuss>();
           
            
            if(json==null && deg==null) //if both the degree and city filer are not sent
            {
                return GetCampuses();       //if both degrees and cities are set to all
            }
            else if(json!=null && deg==null)        //if only city filter is sent
            {
                list = obj.GetCampusesByCity(json);
            }
            else if(json==null && deg!=null)        //if only degree filter is sent
            {
                List<UniversityCampuss> ls2 = obj.GetAllCampuses();
                foreach (UniversityCampuss uc in ls2)      //if there is any degree in the current campus
                {                                          //matches degree from list add it to new list 
                    foreach (int degreeId in deg)
                    {
                        if (uc.CampusDegrees.Any(z => z.DegreeId == degreeId))
                        {
                            list.Add(uc);
                        }
                    }
                }
            }
            else if(json!=null && deg!=null)        //if both the city and degree filter are sent
            {
                List<UniversityCampuss> ls2 = obj.GetCampusesByCity(json);
                foreach (UniversityCampuss uc in ls2)      //if there is any degree in the current campus
                {                                          //matches degree from list add it to new list 
                    foreach (int degreeId in deg)
                    {
                        if (uc.CampusDegrees.Any(z => z.DegreeId == degreeId))
                        {
                            list.Add(uc);
                        }
                    }
                }
            }
            
            var ob = list.Select(campus => new
            {
                campus.Id,
                campus.CampusTitle,
                campus.Address,
                campus.UniversityId,
                campus.AlternateName,
                campus.WebAddress,
                University = new
                {
                    campus.University.Name,
                    campus.University.Id,
                    campus.University.AlternateName,
                    campus.University.Sector,
                    Category = new
                    {
                        Id = campus.University.Category.Id,
                        Name = campus.University.Category.Name,
                        Description = campus.University.Category.Description
                    }
                },
                City = new
                {
                    campus.City.Id,
                    campus.City.Province,
                    campus.City.Name
                },
            });

            return ob.ToList();
        }


        public object GetCampuses()
        {
            UserUniversity obj = new UserUniversity();
            List<UniversityCampuss> list = obj.GetAllCampuses();
            var ob = list.Select(campus => new
            {
                campus.Id,
                campus.CampusTitle,
                campus.Address,
                campus.UniversityId,
                campus.AlternateName,
                campus.WebAddress,
                University = new
                {
                    campus.University.Name,
                    campus.University.Id,
                    campus.University.AlternateName,
                    campus.University.Sector,
                    Category = new 
                         {
                             Id = campus.University.Category.Id,
                             Name = campus.University.Category.Name,
                             Description = campus.University.Category.Description
                         }
                },
                City = new
                {
                    campus.City.Id,
                    campus.City.Province,
                    campus.City.Name
                },
            });

            return ob;
        }



        public object GetUserSubscribedUniversities(int id)     //id is user's id
        {
            UserUniversity us = new UserUniversity();
            return us.GetSubscriptionsWithUniversityDetails(id);
        }


        public object GetUserSubscriptions(int id)
        {
            UserUniversity us = new UserUniversity();
            return us.GetSubscribedUniversities(id);
        }

        public object GetUniversityDetails(int id)
        {
            UserUniversity obj = new UserUniversity();
            University uni = obj.GetUniversityDetail(id);

            List<string>imgList = new List<string>();

            var ob = uni.UniversityCampusses.Select(x => new 
            {
                x.Id,
                x.CampusTitle,
                x.AlternateName,
                x.Address,
                x.WebAddress,
                ImageList = GetImageList(x.Id),
                City = new 
                {
                    Id = x.City.Id,
                    Name = x.City.Name,
                    Province = x.City.Province
                } 
            }).ToArray();


            return new
            {
                uni.Id,
                uni.Name,
                uni.Priority,
                uni.Sector,
                uni.AlternateName,
                uni.WebAddress,
                uni.Description,
                 Category = new 
                         {
                             Id =  uni.Category.Id,
                             Name = uni.Category.Name,
                             Description = uni.Category.Description
                         },
                UniversityCampusses = ob
            };
        }


        public object GetUniversityCampus(int id)
        {
            UserUniversity obj = new UserUniversity();
            UniversityCampuss campus = obj.GetCampusDetail(id);

            //return campus;

            var ob = campus.CampusDegrees.Select(x => new
            {
                x.DegreeSystem,
                Degree = new
                    {
                        Id = x.Degree.Id,
                        x.Degree.DegreeName,
                        x.Degree.ShortName,
                        x.Degree.DegreeDuration,
                        DegreeCareers = x.Degree.DegreeCareers.Select(y => new
                        {
                            y.CareerId,
                            y.DegreeId,
                            Career = new
                                {
                                    y.Career.Id,
                                    y.Career.CareerName,
                                    y.Career.Details
                                }
                        })
                    }
            }).ToList();

            return new
            {
                campus.Id,
                campus.CampusTitle,
                campus.Address,
                campus.UniversityId,
                campus.AlternateName,
                campus.WebAddress,
                ImageList = GetImageList(campus.Id),
                //navigation properties, in order to get rid of circular references
                University = new
                {
                    campus.University.Name,
                    campus.University.Id,
                    campus.University.Priority,
                    campus.University.Description,
                    campus.University.AlternateName,
                    campus.University.Sector,
                    campus.University.WebAddress,
                    Category = new
                    {
                        Id = campus.University.Category.Id,
                        Name = campus.University.Category.Name,
                        Description = campus.University.Category.Description
                    }
                },
                City = new
                {
                    campus.City.Id,
                    campus.City.Province,
                    campus.City.Name
                },

                CampusDegrees = ob
            };

        }


        public object GetUniversitiesNews()
        {
            var code = new UserUniversity().GetNews();

            var query = code.Select(x => new
            {
                x.Title,
                x.Date,
                x.Description,
                x.Id,
                x.UniversityId,
                University = new 
                { 
                    x.University.AlternateName,
                    x.University.Description, 
                    x.University.Id,
                    x.University.Priority,
                    x.University.WebAddress,
                    x.University.Sector,
                    x.University.Name,
                    Category = new
                    {
                        Id = x.University.Category.Id,
                        Name = x.University.Category.Name,
                        Description = x.University.Category.Description
                    },
                }

            });

            return query;
        }

        public object GetUniversityNews(int id)
        {
            var code = new UserUniversity().GetUniversityNews(id);

            var query = code.Select(x => new
            {
                x.Title,
                x.Date,
                x.Description,
                x.Id,
                x.UniversityId,
                University = new
                {
                    x.University.AlternateName,
                    x.University.Description,
                    x.University.Id,
                    x.University.Priority,
                    x.University.WebAddress,
                    x.University.Sector,
                    x.University.Name,
                    Category = new 
                         {
                             Id =  x.University.Category.Id,
                             Name = x.University.Category.Name,
                             Description = x.University.Category.Description
                         }
                }

            });

            return query;
        }



        //filtering news
        [HttpPost]
        public object GetFilteredNews(JObject jobject)
        {
            JsonSerializer serializer = new JsonSerializer();
            NewsFilter ust = (NewsFilter)serializer.Deserialize(new JTokenReader(jobject), typeof(NewsFilter));
            
            int? pageId;
            if (ust.pageId == null)
                pageId = 1;
            else
                pageId = ust.pageId;

            
            
            DateTime dtFrom = DateTime.Now;
            DateTime dtTo = DateTime.Now;
            

            if(ust.dateFrom!=null && ust.dateFrom!="")
                dtFrom = DateTime.Parse(ust.dateFrom);


            if (ust.dateTo != null && ust.dateTo != "")
                dtTo = DateTime.Parse(ust.dateTo);


            var code = new List<UniversityNewss>();
            if(ust.uniId==null || ust.uniId.Count==0)
            {
                code = new UserUniversity().GetNews().OrderByDescending(x=>x.Date).ToList();
            }
            else
            {
                code = new UserUniversity().GetNews().Join(ust.uniId, x => x.UniversityId, y => y, (x, y) => x).OrderByDescending(x=>x.Date).ToList(); 
            }

            //checking filters of date range
            if (ust.dateFrom != "" && ust.dateTo != "")
            {
                code = code.Where(x => x.Date >= dtFrom && x.Date <= dtTo).ToList();
            }
            else if (ust.dateFrom == "" && ust.dateTo != "")
            {
                code = code.Where(x => x.Date <= dtTo).ToList();

            }
            else if (ust.dateFrom != "" && ust.dateTo == "")
            {
                code = code.Where(x => x.Date >= dtFrom).ToList();
            }
            else if (ust.dateFrom == "" && ust.dateTo == "")
            {

            }


            int total = code.Count;

            int count = (int)pageId - 1;
            code = code.Skip(count * 10).Take(10).ToList();


            var mm = code.Select(x => new
            {
                x.Title,
                x.Date,
                x.Description,
                x.Id,
                x.UniversityId,
                University = new
                {
                    x.University.AlternateName,
                    x.University.Description,
                    x.University.Id,
                    x.University.Priority,
                    x.University.WebAddress,
                    x.University.Sector,
                    x.University.Name,
                    Category = new
                    {
                        Id = x.University.Category.Id,
                        Name = x.University.Category.Name,
                        Description = x.University.Category.Description
                    }
                }

            });

            var cc = new
            {
                list = mm,
                count = total
            };

            return cc;
            
        }



        public bool AddSubscription(JObject json)
        {
            JsonSerializer serializer = new JsonSerializer();
            UserSubscriptionToUniversity ust = (UserSubscriptionToUniversity)serializer.Deserialize(new JTokenReader(json), typeof(UserSubscriptionToUniversity));
           
            UserUniversity uu = new UserUniversity();
            if(!uu.GetUserSubscription(ust))
            {
                return uu.AddUserSubscription(ust);
            }

            return true;
        }

        public bool DelSubscription(JObject json)
        {
            JsonSerializer serializer = new JsonSerializer();
            UserSubscriptionToUniversity ust = (UserSubscriptionToUniversity)serializer.Deserialize(new JTokenReader(json), typeof(UserSubscriptionToUniversity));
           
            UserUniversity uu = new UserUniversity();
            if (uu.GetUserSubscription(ust))
            {
                return uu.DeleteUserSubscription(ust);
            }

            return true;
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public object UserSubscribedNews(int id) // id is user id
        {
            var code = new UserUniversity().UserSubscribedNews(id);

            var query = code.Select(x => new
            {
                x.Title,
                x.Date,
                x.Description,
                x.Id,
                x.UniversityId,
                University = new
                {
                    x.University.AlternateName,
                    x.University.Description,
                    x.University.Id,
                    x.University.Priority,
                    x.University.WebAddress,
                    x.University.Sector,
                    x.University.Name,
                    Category = new
                    {
                        Id = x.University.Category.Id,
                        Name = x.University.Category.Name,
                        Description = x.University.Category.Description
                    }
                }

            });

            return query;
        }



        //getting list of image for this universityCampus
        private List<string> GetImageList(int cId)
        {
            string connString = ConfigurationManager.ConnectionStrings["AzureStorageAccount"].ConnectionString;
            string destContainer = ConfigurationManager.AppSettings["destContainer"];

            //Get a reference to the storage account
            CloudStorageAccount sa = CloudStorageAccount.Parse(connString);
            CloudBlobClient bc = sa.CreateCloudBlobClient();

            // Get a reference to the container (creating it if necessary)
            CloudBlobContainer container = bc.GetContainerReference(destContainer);

            string pref = cId + "_";

            var query = container.ListBlobs(prefix: pref);

            var fileName = "";

            List<string> images = new List<string>();
            foreach (var item in query)
            {
                fileName = item.Uri + "";
                fileName = fileName.Replace("https://mustqbilstorage.blob.core.windows.net/campuses/", "");
                images.Add(fileName);
            }

            return images;
        }
    }
}
