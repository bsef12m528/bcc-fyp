﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Net;
using System.Net.Http;
namespace EntityDataFirstExample
{
   public class JsonNetFormatter : MediaTypeFormatter
   {
       private JsonSerializerSettings _jsonSerializerSettings;
    
       public JsonNetFormatter(JsonSerializerSettings jsonSerializerSettings)
       {
           _jsonSerializerSettings = jsonSerializerSettings ?? new JsonSerializerSettings();

           SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));

       }
    
       public override bool CanReadType(Type type)
       {      
    
           return true;
       }
    
       public override bool CanWriteType(Type type)
       {
           return true;
       }

       public override Task<object> ReadFromStreamAsync(Type type, Stream stream, HttpContent contentHeaders, IFormatterLogger formatterContext)
       {
           // Create a serializer
           JsonSerializer serializer = JsonSerializer.Create(_jsonSerializerSettings);
    
           // Create task reading the content
           return Task.Factory.StartNew(() =>
           {
               using (StreamReader streamReader = new StreamReader(stream, new UTF8Encoding(false, true)))
               {
                   using (JsonTextReader jsonTextReader = new JsonTextReader(streamReader))
                   {
                       return serializer.Deserialize(jsonTextReader, type);
                   }
               }
           });
       }
    
       public override Task WriteToStreamAsync(Type type, object value, Stream stream, HttpContent contentHeaders, TransportContext transportContext)
       {
           // Create a serializer
           JsonSerializer serializer = JsonSerializer.Create(_jsonSerializerSettings);
    
           // Create task writing the serialized content
          return Task.Factory.StartNew(() =>
           {
               JsonTextWriter jsonTextWriter = new JsonTextWriter(new StreamWriter(stream, new UTF8Encoding(false, true))) { CloseOutput = false };
               
                   serializer.Serialize(jsonTextWriter, value);
                   jsonTextWriter.Flush();
               
           });
       }
   }
}
