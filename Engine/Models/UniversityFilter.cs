﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Engine.Models
{
    public class UniversityFilter
    {
        public List<int> DegreeIds { get; set; }
        public List<int> CityIds { get; set; }
    }
}