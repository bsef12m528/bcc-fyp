﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Engine.Models
{
    public class NewsFilter
    {
        public List<int> uniId { get; set; }
        public string dateFrom { get; set; }
        public string dateTo { get; set; }
        public int? pageId { get; set; }
    }
}