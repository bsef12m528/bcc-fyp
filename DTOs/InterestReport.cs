﻿namespace DTOs
{
    using System;
    using System.Collections.Generic;

    public class InterestReport
    {
        public string InterestSummary { get; set; }
        public string PersonalitySummary { get; set; }
        public List<SelectedCareers> SelectedCareers { get; set; }
    }

    public class SelectedCareers
    {
        public int Id { get; set; }
        public string CareerName { get; set; }
        public string Details { get; set; }
    }
}
