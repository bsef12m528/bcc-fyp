﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOs
{
    public class CareerResult
    {
        public int CareerId { get; set; }
        public int TotalPoints { get; set; }
        public int? PersonalityTypeId { get; set; }
    }

}
