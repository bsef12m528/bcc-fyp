﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DTOs
{
    public class UniversityWithSession
    {
        [DataMember()]
        public Int32 Id { get; set; }

        [DataMember()]
        public String Sector { get; set; }

        [DataMember()]
        public String Name { get; set; }

        [DataMember()]
        public String Description { get; set; }

        [DataMember()]
        public Nullable<Int32> Priority { get; set; }

        [DataMember()]
        public String AlternateName { get; set; }

        [DataMember()]
        public String WebAddress { get; set; }

        [DataMember()]
        public Category Category { get; set; }

        [DataMember()]
        public Nullable<Int32> CategoryId { get; set; }

        public bool IsSubscribed { get; set; }
    }
}