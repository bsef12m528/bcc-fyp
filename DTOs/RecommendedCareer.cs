//-------------------------------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by EntitiesToDTOs.v3.2 (entitiestodtos.codeplex.com).
//     Timestamp: 2016/04/01 - 15:26:17
//
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//-------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace DTOs
{
    public partial class RecommendedCareer
    {
        public Int32 Id { get; set; }

        public Nullable<Int32> UserTestReportId { get; set; }

        public Nullable<Int32> CareerId { get; set; }

        public Career Career { get; set; }

        public UserTestReport UserTestReport { get; set; }

        public RecommendedCareer()
        {
        }

        public RecommendedCareer(Int32 id, Nullable<Int32> userTestReportId, Nullable<Int32> careerId, Career career, UserTestReport userTestReport)
        {
			this.Id = id;
			this.UserTestReportId = userTestReportId;
			this.CareerId = careerId;
			this.Career = career;
			this.UserTestReport = userTestReport;
        }
    }
}
