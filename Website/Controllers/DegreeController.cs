﻿using DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Website.Controllers
{
    public class DegreeController : Controller
    {

        
        //
        // GET: /Degree/

        public ActionResult AllDegrees()
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;


            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();

            var json = client.DownloadString(engineUrl + "api/Degree/GetAllDegrees");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Degree> c = serializer.Deserialize< List<Degree> >(json);
            ViewBag.degrees = c;

            return View();
        }

        public ActionResult UniversitiesInDegree(int id)
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;


            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
           
            var json = client.DownloadString(engineUrl + "api/Degree/GetDegree/" + id);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Degree c = serializer.Deserialize<Degree>(json);
            ViewBag.d = c;

            return View();
        }
	}
}