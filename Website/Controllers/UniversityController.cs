﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTOs;
using System.Net;
using System.Web.Script.Serialization;
using System.Diagnostics;
using System.Web.Configuration;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using Website.Models;


namespace Website.Controllers
{
    public class UniversityController : Controller
    {
        //
        // GET: /University/
        public ActionResult Index()
        {
            if (Session["user"] != null)
            {
                return RedirectToAction("UniversitiesWithSession", "University");
            }

            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            //json string from url

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/University/GetUniversities");


            //dynamic search = System.Web.Helpers.Json.Decode(json);
            //Debug.WriteLine(json.ToString());

            //convert json array to object array
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<University> uni = serializer.Deserialize<List<University>>(json);

            ViewBag.list = uni.OrderBy(z=>z.Name);

            return View();
        }

        public ActionResult UniversityDetails(int id)
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl+ "api/University/GetUniversityDetails/" + id);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            University uni = serializer.Deserialize<University>(json);

            if(!uni.WebAddress.Contains("http"))
            {
                uni.WebAddress = "http://" + uni.WebAddress;
            }

           
            ViewBag.uni = uni;
            return View();
        }

        public ActionResult UniversitiesBySector(string sector)
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/University/GetUniversitiesBySector?json=" + sector);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<University> uni = serializer.Deserialize<List<University>>(json);

            ViewBag.list = uni;
            return View();
        }

        public ActionResult CampusDetails(int id)
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/University/GetUniversityCampus/" + id);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            UniversityCampuss campus = serializer.Deserialize<UniversityCampuss>(json);

            if (campus.WebAddress!=null && !campus.WebAddress.Contains("http")  && campus.WebAddress!="")
            {
                campus.WebAddress = "http://" + campus.WebAddress;
            }


            ViewBag.campus = campus;

            return View();
        }

        public ActionResult UniversitiesWithSession()
        {
            User usr = (User)Session["user"];

            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/University/GetUserSubscribedUniversities/" + usr.Id);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<UniversityWithSession> list = serializer.Deserialize< List<UniversityWithSession> >(json);

            ViewBag.list = list;
            return View();
        }

        public ActionResult SubscribedUniversities()
        {
            User usr = (User)Session["user"];

            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/University/GetUserSubscriptions/" + usr.Id);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<UniversityWithSession> list = serializer.Deserialize<List<UniversityWithSession>>(json);

            ViewBag.list = list.OrderBy(z=>z.Name);
            return View();
        }

        public ActionResult News()
        {
            //getting all news
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;
            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/University/GetUniversitiesNews");
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<UniversityNewss> list = serializer.Deserialize< List<UniversityNewss> >(json);

            //getting all universities
            client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;
            engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            json = client.DownloadString(engineUrl + "api/University/GetUniversities");
            serializer = new JavaScriptSerializer();
            List<University> uni = serializer.Deserialize<List<University>>(json);

            //making select box for uni list
            List<SelectListItem> uniList = new List<SelectListItem>();
            foreach (var a in uni.OrderBy(z=>z.Name))
            {
                uniList.Add(new SelectListItem { Text = a.Name + "(" + a.Sector + ")", Value = a.Id.ToString(), Selected = false });
            }


            ViewBag.uniList = uniList;
            ViewBag.list = list;

            return View();
        }

        public ActionResult FilteredNews(List<int>arr , string dateFrom , string dateTo, int? pgNo)
        {
            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(engineUrl + "api/University/GetFilteredNews");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";


            //sending data for request
            var myWriter = new StreamWriter(httpWebRequest.GetRequestStream());


            if (pgNo == null)
                pgNo = 1;

            var jobject = new
            {
                uniId = arr,
                dateFrom = dateFrom,
                dateTo = dateTo,
                pageId = pgNo
            };

            var zz = new JavaScriptSerializer().Serialize(jobject);

            myWriter.Write(zz);  //since questions is already in json format
            myWriter.Close();

            var webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
            Stream receiveStream = webResponse.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            //This is webresponse whole data. 
            string responseData = readStream.ReadToEnd();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            NewsFilterClass nfc = serializer.Deserialize<NewsFilterClass>(responseData);

            ViewBag.list = nfc.list;
            ViewBag.thisPage = pgNo;
            ViewBag.count = nfc.count;

            webResponse.Close();
            readStream.Close();

            return PartialView("FilteredNews");
        }

        public ActionResult UniversityNews(int id)
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/University/GetUniversityNews/" + id);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<UniversityNewss> list = serializer.Deserialize<List<UniversityNewss>>(json);

            ViewBag.list = list;
            ViewBag.uniName = list.First().University.Name;

            return View();
        }

        public object AddSubscription(UserSubscriptionToUniversity ust)
        {
            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(engineUrl + "api/University/AddSubscription");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";


            //sending data for request
            var myWriter = new StreamWriter(httpWebRequest.GetRequestStream());
            var json = new JavaScriptSerializer().Serialize(ust);
            myWriter.Write(json); 
            myWriter.Close();



            var webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
            Stream receiveStream = webResponse.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            //This is webresponse whole data. 
            string responseData = readStream.ReadToEnd();

            webResponse.Close();
            readStream.Close();

            return responseData;
        }

        public object DeleteSubscription(UserSubscriptionToUniversity ust)
        {
            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(engineUrl + "api/University/DelSubscription");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";


            //sending data for request
            var myWriter = new StreamWriter(httpWebRequest.GetRequestStream());
            var json = new JavaScriptSerializer().Serialize(ust);
            myWriter.Write(json);  //since questions is already in json format
            myWriter.Close();



            var webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
            Stream receiveStream = webResponse.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            //This is webresponse whole data. 
            string responseData = readStream.ReadToEnd();

            webResponse.Close();
            readStream.Close();

            return responseData;

        }

        public ActionResult UserSubscribedNews()
        {
            User usr = (User)Session["user"];

            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/University/UserSubscribedNews/" + usr.Id );

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<UniversityNewss> list = serializer.Deserialize<List<UniversityNewss>>(json);

            ViewBag.list = list;


            return View();

        }


        public ActionResult AllCampuses()
        {
            List<City> cities = GetCitiesList();
            List<SelectListItem> list2 = new List<SelectListItem>();
            foreach(var a in cities.OrderBy(z=>z.Name))
            {
                list2.Add(new SelectListItem { Text = a.Name, Value = a.Id.ToString(), Selected = false });
            }
            ViewBag.Cities = list2;

            List<Degree> degrees = GetDegreeList();
            List<SelectListItem> list3 = new List<SelectListItem>();
            foreach (var a in degrees.OrderBy(z=>z.DegreeName))
            {
                list3.Add(new SelectListItem { Text = a.DegreeName + "(" + a.DegreeDuration +  " years)"  , Value = a.Id.ToString(), Selected = false });
            }

            ViewBag.Degrees = list3;


            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/University/GetCampuses");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<UniversityCampuss> list = serializer.Deserialize<List<UniversityCampuss>>(json);
            
            ViewBag.list = list.OrderBy(z=>z.University.Name);
            return View();

        }

        public ActionResult UniversitiesInCities(List<string> arr , List<string>err)
        {
            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            //return "";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(engineUrl + "api/University/GetCampusesInCities");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";


            //sending data for request
            var myWriter = new StreamWriter(httpWebRequest.GetRequestStream());
          
            var jobject = new
            {
                CityIds = arr,
                DegreeIds = err
            };


            var zz = new JavaScriptSerializer().Serialize(jobject);
            myWriter.Write(zz);
            myWriter.Close();



            var webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
            Stream receiveStream = webResponse.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            //This is webresponse whole data. 
            string responseData = readStream.ReadToEnd();

            webResponse.Close();
            readStream.Close();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<UniversityCampuss> list = serializer.Deserialize<List<UniversityCampuss>>(responseData);

            return PartialView("UniversitiesInCities", list.OrderBy(z=>z.University.Name));
        }

         private List<City> GetCitiesList()
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString(engineUrl + "api/City/GetCities");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<City> list = serializer.Deserialize<List<City>>(json);
            return list;
        }

        private List<Degree> GetDegreeList()
         {
             var client = new WebClient();
             client.Encoding = System.Text.Encoding.UTF8;

             string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
             var json = client.DownloadString(engineUrl + "api/Degree/GetAllDegrees");

             JavaScriptSerializer serializer = new JavaScriptSerializer();
             List<Degree> list = serializer.Deserialize<List<Degree>>(json);
             return list;
         }



    }
}