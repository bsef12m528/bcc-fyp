﻿using DTOs;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Website.Controllers
{
    public class QuestionnaireController : Controller
    {
        //
        // GET: /Questionnaire/
        public ActionResult Instructions()
        {
            return View();
        }


        public ActionResult Quiz()
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString( engineUrl + "api/Question/GetQuestions");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Question> list = serializer.Deserialize<List<Question>>(json);

            var rnd = new Random();
            list = list.OrderBy(x => rnd.Next()).ToList();

            ViewBag.list = list.ToList();
            return View();
        }

        [HttpPost]
        public object GetAttemptedQuiz(string questions)
        {
            try
            {

                string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
                //making a web request to webservice
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(engineUrl + "api/Question/GetResult");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";


                //sending data for request
                var myWriter = new StreamWriter(httpWebRequest.GetRequestStream());
                //var json = new JavaScriptSerializer().Serialize(questions);
                myWriter.Write(questions);  //since questions is already in json format
                myWriter.Close();



                var webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
                Stream receiveStream = webResponse.GetResponseStream();
                // Pipes the stream to a higher level stream reader with the required encoding format. 
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                //This is webresponse whole data. 
                string responseData = readStream.ReadToEnd();

                webResponse.Close();
                readStream.Close();

                var json = responseData;

                JavaScriptSerializer oJS = new JavaScriptSerializer();
                InterestReport oRootObject = new InterestReport();
                oRootObject = oJS.Deserialize<InterestReport>(json);


                if (Session["user"] != null)
                {
                    SaveReport(oRootObject);
                }
                TempData["Report"] = oRootObject;

                return "success";
            }
            catch (Exception exc)
            {

                return exc.Message;
            }



        }


        private void SaveReport(InterestReport ir)
        {
            

            UserTestReport utr = new UserTestReport();
            utr.ReportSummary = ir.PersonalitySummary + "<br>" + ir.InterestSummary;
            utr.UserId = ((User)Session["user"]).Id;


            utr.RecommendedCareers = new List<RecommendedCareer>();
            foreach (SelectedCareers c in ir.SelectedCareers)
            {
                utr.RecommendedCareers.Add(new RecommendedCareer
                {
                    CareerId = c.Id
                });
            }

            //making a web request to webservice

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(engineUrl + "api/Report/SaveReport");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";


            //sending data for request
            var myWriter = new StreamWriter(httpWebRequest.GetRequestStream());
            var json = new JavaScriptSerializer().Serialize(utr);
            myWriter.Write(json);  //since questions is already in json format
            myWriter.Close();



            var webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
            Stream receiveStream = webResponse.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            //This is webresponse whole data. 
            string responseData = readStream.ReadToEnd();

            webResponse.Close();
            readStream.Close();

        }


	}
}

