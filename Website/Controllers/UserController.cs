﻿using DTOs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace Views.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        public ActionResult Index()
        {
            return View();
        }

        public object Signin(User user)
        {
            //making a web request to webservice

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(engineUrl + "api/User/GetSignin");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            //sending data for request
            var myWriter = new StreamWriter(httpWebRequest.GetRequestStream());
            var json = new JavaScriptSerializer().Serialize(user);
            myWriter.Write(json);
            myWriter.Close();



            var webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
            Stream receiveStream = webResponse.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            //This is webresponse whole data. 
            string responseData = readStream.ReadToEnd();
            User us = new JavaScriptSerializer().Deserialize<User>(responseData);

            bool isRegistered = (us != null);

            webResponse.Close();
            readStream.Close();

          
            if (isRegistered)
            {
                Session["user"] = us;

                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = true }
                };
            }
            else
            {
                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = false }
                };
            }
        }

        public object Signup(User user)
        {
            //making a web request to webservice

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(engineUrl+"api/User/GetSignup");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            //sending data for request
            var myWriter = new StreamWriter(httpWebRequest.GetRequestStream());
            var json = new JavaScriptSerializer().Serialize(user);
            myWriter.Write(json);
            myWriter.Close();



            var webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
            Stream receiveStream = webResponse.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            //This is webresponse whole data. 
            string responseData = readStream.ReadToEnd();

            webResponse.Close();
            readStream.Close();


            //In this case i am checking if responseData=="true" than return success message else error message

            bool isRegistered = Convert.ToBoolean(responseData);
            if (isRegistered)
            {
                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = true }
                };
            }
            else
            {
                return new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new { result = false }
                };
            }
        }

        public ActionResult Logout()
        {
            if(Session["user"]!=null)
            {
                Session["user"] = null;
            }
            return RedirectToAction("Index", "Home");
        }


        public ActionResult ViewProfile()
        {
            User us = (User)Session["user"];
            

            return View(us);
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        public object ChangePasswordResponse(string oldPassword,string newPassword)
        {
             User uss = (User)Session["user"];

             if (oldPassword == uss.Password)
             {
                 uss.Password = newPassword;

                 string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
                 var httpWebRequest = (HttpWebRequest)WebRequest.Create(engineUrl + "api/User/UpdateUser");
                 httpWebRequest.ContentType = "application/json";
                 httpWebRequest.Method = "POST";

                 //sending data for request
                 var myWriter = new StreamWriter(httpWebRequest.GetRequestStream());
                 var json = new JavaScriptSerializer().Serialize(uss);
                 myWriter.Write(json);
                 myWriter.Close();



                 var webResponse = httpWebRequest.GetResponse() as HttpWebResponse;
                 Stream receiveStream = webResponse.GetResponseStream();
                 // Pipes the stream to a higher level stream reader with the required encoding format. 
                 StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                 //This is webresponse whole data. 
                 string responseData = readStream.ReadToEnd();
                 responseData = new JavaScriptSerializer().Deserialize<string>(responseData);

               
                 webResponse.Close();
                 readStream.Close();

                 return new JsonResult()
                 {
                     JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                     Data = responseData
                 };

             }
             else
             {
                 return new JsonResult()
                 {
                     JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                     Data = "Incorrect current Password entered."
                 };
             }
        }

    }
}