﻿using DTOs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Website.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/
     
        public ActionResult UserReports()
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();

            User us = (User)Session["user"];

            var json = client.DownloadString(engineUrl + "api/Report/GetReportOfUser/" + us.Id);
            
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<UserTestReport> l = serializer.Deserialize< List<UserTestReport> >(json);
            ViewBag.list = l;
            return View();

        }


        public ActionResult ViewReport(int id)
        {
            var client = new WebClient();

            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();

            //User us = (User)Session["user"];

            var json = client.DownloadString(engineUrl + "api/Report/GetReport/" + id);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            UserTestReport report = serializer.Deserialize<UserTestReport>(json);
            ViewBag.Report = report;

            Random rnd = new Random();
            int num = rnd.Next(1, 6);
            ViewBag.image = "career_" + num + ".jpg";


            return View();
        }



        public ActionResult PersonalitySummary()
        {
            // if (TempData["doc"] != null)
            // {
            InterestReport rpt = (InterestReport)TempData["Report"];
            ViewBag.Report = rpt;
            TempData["Report"] = rpt;

            return View();
            //}


        }



        public ActionResult FullReport()
        {
            InterestReport rpt = (InterestReport)TempData["Report"];
            ViewBag.Report = rpt;

            return View();
        }





	}
}