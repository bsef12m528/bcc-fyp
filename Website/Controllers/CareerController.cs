﻿using DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Website.Controllers
{
    public class CareerController : Controller
    {
        //
        // GET: /Career/
        public ActionResult DegreesInCareer(int id)
        {
            var client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;


            string engineUrl = WebConfigurationManager.ConnectionStrings["EngineUrl"].ToString();
            var json = client.DownloadString( engineUrl + "api/Career/GetCareer/" + id);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Career c = serializer.Deserialize<Career>(json);

            ViewBag.c = c;

            //ViewBag.list = list.Where(x => x.Id == 1 || x.Id == 2 || x.Id == 3 || x.Id == 4 || x.Id == 14 || x.Id == 15).ToList();
            return View();
        }


	}
}