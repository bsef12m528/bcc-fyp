﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface ICrawler
{
    HtmlDocument CrawlDocument();
    void CrawlSingleNews(string url);   //this page will have single news and there we will perform operation 
    //and save the single news to object
    void CrawlAllNews(HtmlDocument document);      //this page will be having all the news
    void Execute();
}
