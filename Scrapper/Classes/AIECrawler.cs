﻿using DAL.Classes;
using DAL.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Classes
{
    public class AIECrawler : ICrawler
    {
        List<UniversityNewss> list;
        public AIECrawler()
        {
            list = new List<UniversityNewss>();
        }


        public HtmlDocument CrawlDocument()
        {
            var crawledPage = new HtmlDocument();

            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            crawledPage.LoadHtml(client.DownloadString("http://www.aie.edu.pk/"));
            HtmlAgilityPack.HtmlDocument ap = crawledPage;
            return crawledPage;
        }


        public void CrawlAllNews(HtmlDocument document)
        {
            HtmlNodeCollection nodes = document.DocumentNode.SelectNodes(@"//div[@class='ja-slidenews-item']");
            var x = nodes.Descendants("a")
                    .Select(a => a.GetAttributeValue("href", string.Empty))
                    .ToList();


            //Console.WriteLine("Found  total of " + x.Count + " news on the website");
            foreach (var y in x)
            {
                //Console.WriteLine("Crawling url ' " + y + " ' ");
                CrawlSingleNews("http://www.aie.edu.pk" + y);
            }

        }


        public void CrawlSingleNews(string url)
        {
            try
            {
                var crawledPage = new HtmlDocument();
                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;


                crawledPage.LoadHtml(client.DownloadString(url));
                HtmlAgilityPack.HtmlDocument ap = crawledPage;

                HtmlNode titleNode = ap.DocumentNode.SelectSingleNode(@"//h2[@class='contentheading']/a");
                string title = CleanAndTrim(titleNode.InnerText);


                HtmlNode dateNode = ap.DocumentNode.SelectSingleNode("//dd[@class='published']/span");
                string date = CleanAndTrim(dateNode.InnerText);


                DateTime dt = DateTime.Parse(date);
                date = dt.ToString("MM/dd/yyyy");

                string description = "";
                var descriptionNodes = ap.DocumentNode.SelectNodes("//div[@class='item-page clearfix']/p");
                foreach(HtmlNode nd in descriptionNodes)
                {
                    description+=CleanAndTrim(nd.InnerText);
                }


                UniversityNewss un = new UniversityNewss
                {
                    Title = title,
                    Date = dt,
                    Description = description
                };

                list.Add(un);
            }
            catch (Exception exc)
            {

            }



        }

        public void Execute()
        {
            try
            {

                HtmlDocument doc = CrawlDocument();
                CrawlAllNews(doc);
                InsertToDb();

            }
            catch (Exception)
            {
                
                
            }
        }

        public void InsertToDb()
        {
            AdminNewsFeed anf = new AdminNewsFeed();
            anf.AddNews(this.list, "aie");
            //Console.WriteLine( anf.AddNews(this.list, "aku"));
        }


        private string CleanAndTrim(string abc)
        {
            abc = abc.Replace("\n", "");
            abc = abc.Replace("\t", "");
            abc = abc.Replace("<br>", "");
            abc = abc.Replace("\r", "");
            abc = abc.Replace("\\", "");
            abc = abc.Trim();
            return abc;
        }

    }
}
