﻿using DAL.Classes;
using DAL.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Classes
{
    public class AUCrawler : ICrawler
    {
        List<UniversityNewss> list;
        public AUCrawler()
        {
            list = new List<UniversityNewss>();
        }


        public HtmlDocument CrawlDocument()
        {
            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            var crawledPage = new HtmlDocument();
            crawledPage.LoadHtml(client.DownloadString("http://portals.au.edu.pk/auinfo/showpages/NewsDetails.aspx"));
            HtmlAgilityPack.HtmlDocument ap = crawledPage;
            return crawledPage;
        }


        public void CrawlAllNews(HtmlDocument document)
        {
            HtmlNodeCollection nodes = document.DocumentNode.SelectNodes(@"//table[@id='ctl00_ContentPlaceHolder1_DataList1']//tr");

            for (int i = 1; i < nodes.Count; i=i+3 )
            {
                HtmlNodeCollection upperNode = nodes[i].SelectNodes("td");  //keeping title & date 
                string title = CleanAndTrim(upperNode[0].InnerText);
                string date = CleanAndTrim(upperNode[1].InnerText);
                string description = CleanAndTrim(nodes[i+1].InnerText);


                DateTime dt = DateTime.Parse(date);
                date = dt.ToString("MM/dd/yyyy");
                UniversityNewss un = new UniversityNewss
                {
                    Description = description,
                    Title = title,
                    Date = dt
                };
                list.Add(un);
            }
               
     
        }


        public void CrawlSingleNews(string url)
        {
          

        }

        public void Execute()
        {
            try
            {

                HtmlDocument doc = CrawlDocument();
                CrawlAllNews(doc);
                InsertToDb();

            }
            catch (Exception)
            {


            }

        }

        public void InsertToDb()
        {
            AdminNewsFeed anf = new AdminNewsFeed();
            anf.AddNews(this.list, "au");
            //Console.WriteLine(anf.AddNews(this.list, "au"));
        }


        private string CleanAndTrim(string abc)
        {
            abc = abc.Replace("\n", "");
            abc = abc.Replace("\t", "");
            abc = abc.Replace("<br>", "");
            abc = abc.Replace("\r", "");
            abc = abc.Replace("\\", "");
            abc = abc.Trim();
            return abc;
        }

    }
}
