﻿using DAL.Classes;
using DAL.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Classes
{
    public class AIUCrawler : ICrawler
    {
        List<UniversityNewss> list;
        public AIUCrawler()
        {
            list = new List<UniversityNewss>();
        }


        public HtmlDocument CrawlDocument()
        {
            var crawledPage = new HtmlDocument();

            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            crawledPage.LoadHtml(client.DownloadString("http://www.aiu.edu.pk/TestAIU/"));
            HtmlAgilityPack.HtmlDocument ap = crawledPage;
            return crawledPage;
        }


        public void CrawlAllNews(HtmlDocument document)
        {
            HtmlNodeCollection nodes = document.DocumentNode.SelectNodes(@"//div[@class='ja-slidenews-item']");
            var x = nodes.Descendants("a")
                    .Select(a => a.GetAttributeValue("href", string.Empty))
                    .ToList();


            //Console.WriteLine("Found  total of " + x.Count + " news on the website");
            foreach (var y in x)
            {
                //Console.WriteLine("Crawling url ' " + y + " ' ");
                CrawlSingleNews("http://www.aiu.edu.pk"+ y);
            }

        }


        public void CrawlSingleNews(string url)
        {
            try
            {
                var crawledPage = new HtmlDocument();
                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;


                crawledPage.LoadHtml(client.DownloadString(url));
                HtmlAgilityPack.HtmlDocument ap = crawledPage;

                HtmlNode titleNode = ap.DocumentNode.SelectSingleNode(@"//h2[@class='itemTitle']");
                string title = CleanAndTrim(titleNode.InnerText);


                HtmlNode dateNode = ap.DocumentNode.SelectSingleNode("//span[@class='itemDateCreated']");
                string date = CleanAndTrim(dateNode.InnerText);
               

                DateTime dt = DateTime.Parse(date);
                date = dt.ToString("MM/dd/yyyy");

                string description = "";
                var descriptionNode = ap.DocumentNode.SelectSingleNode("//div[@class='itemFullText']/p[2]");
                description = CleanAndTrim(descriptionNode.InnerText); 

                
                UniversityNewss un = new UniversityNewss
                {
                    Title = title,
                    Date = dt,
                    Description = description
                };

                list.Add(un);
            }
            catch (Exception exc)
            {

            }



        }

        public void Execute()
        {
            try
            {

                HtmlDocument doc = CrawlDocument();
                CrawlAllNews(doc);
                InsertToDb();

            }
            catch (Exception)
            {


            }

        }

        public void InsertToDb()
        {
            AdminNewsFeed anf = new AdminNewsFeed();
            anf.AddNews(this.list, "aiu");
            //Console.WriteLine( anf.AddNews(this.list, "aku"));
        }


        private string CleanAndTrim(string abc)
        {
            abc = abc.Replace("\n", "");
            abc = abc.Replace("\t", "");
            abc = abc.Replace("<br>", "");
            abc = abc.Replace("\r", "");
            abc = abc.Replace("\\", "");
            abc = abc.Trim();
            return abc;
        }

    }
}
