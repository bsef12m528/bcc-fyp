﻿using DAL.Classes;
using DAL.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Classes
{
    public class AIOUCrawler : ICrawler
    {
        List<UniversityNewss> list;
        public AIOUCrawler()
        {
            list = new List<UniversityNewss>();
        }


        public HtmlDocument CrawlDocument()
        {
            var crawledPage = new HtmlDocument();

            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            crawledPage.LoadHtml(client.DownloadString("http://www.aiou.edu.pk/index.asp"));
            HtmlAgilityPack.HtmlDocument ap = crawledPage;
            return crawledPage;
        }


        public void CrawlAllNews(HtmlDocument document)
        {
            HtmlNodeCollection nd = document.DocumentNode.SelectNodes("//a[@href]");

            foreach (HtmlNode link in nd)
            {
                string href = link.GetAttributeValue("href",string.Empty);
                if(href.Contains("NewsDetail.asp?Id"))
                {
                    CrawlSingleNews("http://www.aiou.edu.pk/" + href);
                }
            }


        }


        public void CrawlSingleNews(string url)
        {
            try
            {
                var crawledPage = new HtmlDocument();
                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;


                crawledPage.LoadHtml(client.DownloadString(url));
                HtmlAgilityPack.HtmlDocument ap = crawledPage;

                HtmlNode titleNode = ap.DocumentNode.SelectSingleNode(@"/html[1]/body[1]/table[1]/tr[1]/td[1]/div[1]/center[1]/table[1]/tbody[1]/tr[5]/td[2]");
                string title = CleanAndTrim(titleNode.InnerText);


                HtmlNode dateNode = ap.DocumentNode.SelectSingleNode(@"/html[1]/body[1]/table[1]/tr[1]/td[1]/div[1]/center[1]/table[1]/tbody[1]/tr[4]/td[2]");
                string date = CleanAndTrim(dateNode.InnerText);


                DateTime dt = DateTime.Parse(date);
                date = dt.ToString("MM/dd/yyyy");

                string description = "";
                var descriptionNode = ap.DocumentNode.SelectSingleNode(@"/html[1]/body[1]/table[1]/tr[1]/td[1]/div[1]/center[1]/table[1]/tbody[1]/tr[7]/td[2]/div[1]");
                //foreach (HtmlNode nd in descriptionNodes)
                //{
                //    description += CleanAndTrim(nd.InnerText);
                //}

                description = CleanAndTrim(descriptionNode.InnerText);


                UniversityNewss un = new UniversityNewss
                {
                    Title = title,
                    Date = dt,
                    Description = description
                };

                list.Add(un);
            }
            catch (Exception exc)
            {

            }



        }

        public void Execute()
        {
            try
            {

                HtmlDocument doc = CrawlDocument();
                CrawlAllNews(doc);
                InsertToDb();

            }
            catch (Exception)
            {


            }

        }

        public void InsertToDb()
        {
            AdminNewsFeed anf = new AdminNewsFeed();
            anf.AddNews(this.list, "aiou");
            //Console.WriteLine( anf.AddNews(this.list, "aku"));
        }


        private string CleanAndTrim(string abc)
        {
            abc = abc.Replace("\n", "");
            abc = abc.Replace("\t", "");
            abc = abc.Replace("<br>", "");
            abc = abc.Replace("\r", "");
            abc = abc.Replace("\\", "");
            abc = abc.Trim();
            return abc;
        }

    }
}
