﻿using DAL.Classes;
using DAL.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Scrapper.Classes
{
    public class AKUCrawler : ICrawler
    {
        List<UniversityNewss> list;
        public AKUCrawler()
        {
            list = new List<UniversityNewss>();
        }


        public HtmlDocument CrawlDocument()
        {
            var crawledPage = new HtmlDocument();

            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;

            crawledPage.LoadHtml(client.DownloadString("http://www.aku.edu/aboutaku/news/Pages/NewsListingPage.aspx?type=all"));
            HtmlAgilityPack.HtmlDocument ap = crawledPage;
            return crawledPage;
        }


        public void CrawlAllNews(HtmlDocument document)
        {
            HtmlNodeCollection nodes = document.DocumentNode.SelectNodes(@"//div[@class='news-srch-row']");
            var x = nodes.Descendants("a")
                    .Select(a => a.GetAttributeValue( "href", string.Empty ))
                    .ToList();


            //Console.WriteLine("Found  total of " + x.Count + " news on the website");
            foreach(var y in x)
            {
                //Console.WriteLine("Crawling url ' " + y + " ' ");
                CrawlSingleNews(y);
            }

        }


        public void CrawlSingleNews(string url)
        {
            try
            {
                var crawledPage = new HtmlDocument();
                WebClient client = new WebClient();
                client.Encoding = System.Text.Encoding.UTF8;


                crawledPage.LoadHtml(client.DownloadString(url));
                HtmlAgilityPack.HtmlDocument ap = crawledPage;

                HtmlNode titleNode = ap.DocumentNode.SelectSingleNode(@"/html[1]/head[1]/title[1]");
                string title = titleNode.InnerText.Replace("\t", "");
                title = title.Replace("\n", "");
                title = title.Replace("\r", "");


                HtmlNode dateNode = ap.DocumentNode.SelectSingleNode("//span[@class='ndate']");
                string date = dateNode.InnerText.Replace("\n", "");
                date.Replace("\t", "");
                date.Replace("\r", "");

                DateTime dt = DateTime.Parse(date);
                date = dt.ToString("MM/dd/yyyy");

                string description = "";
                HtmlNodeCollection descriptionNodes = ap.DocumentNode.SelectNodes("//p[@class='ms-rteElement-P']");
                
                foreach (var descriptionNode in descriptionNodes)
                {
                    description = description.Replace("<br>", "");
                    description = description.Replace("<br/>", "");
                    description = description.Replace("\n", "");
                    description = description.Replace("\t", "");
                    description = description.Replace("\r", "");
                    
                    description += " " + descriptionNode.InnerText;
                }

                UniversityNewss un = new UniversityNewss
                {
                    Title = title,
                    Date = dt,
                    Description = description
                };

                list.Add(un);
            }
            catch (Exception)
            {
                
            }



        }

        public void Execute()
        {
            try
            {

                HtmlDocument doc = CrawlDocument();
                CrawlAllNews(doc);
                InsertToDb();

            }
            catch (Exception)
            {


            }

        }

        public void InsertToDb()
        {
            AdminNewsFeed anf = new AdminNewsFeed();
            anf.AddNews(this.list, "aku");
           //Console.WriteLine( anf.AddNews(this.list, "aku"));
        }


        private string CleanAndTrim(string abc)
        {
            abc = abc.Replace("\n", "");
            abc = abc.Replace("\t", "");
            abc = abc.Replace("<br>", "");
            abc = abc.Replace("\r", "");
            abc = abc.Replace("\\", "");
            abc = abc.Trim();
            return abc;
        }

    }
}
