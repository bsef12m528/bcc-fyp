﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DbEntities : DbContext
    {
        public DbEntities()
            : base("name=DbEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Administrator> Administrators { get; set; }
        public virtual DbSet<CampusDegree> CampusDegrees { get; set; }
        public virtual DbSet<Career> Careers { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Degree> Degrees { get; set; }
        public virtual DbSet<DegreeCareer> DegreeCareers { get; set; }
        public virtual DbSet<PersonalityType> PersonalityTypes { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<QuestionCareer> QuestionCareers { get; set; }
        public virtual DbSet<RecommendedCareer> RecommendedCareers { get; set; }
        public virtual DbSet<University> Universities { get; set; }
        public virtual DbSet<UniversityCampuss> UniversityCampusses { get; set; }
        public virtual DbSet<UniversityNewss> UniversityNewsses { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserAcademicBackground> UserAcademicBackgrounds { get; set; }
        public virtual DbSet<UserSubscriptionToUniversity> UserSubscriptionToUniversities { get; set; }
        public virtual DbSet<UserTestReport> UserTestReports { get; set; }
    }
}
