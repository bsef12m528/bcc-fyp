﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class AQuestion
    {
        public int QuestionId { get; set; }
        public int AttemptedAnswer { get; set; }
        public string Statement { get; set; }
    }
}
