//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PersonalityType
    {
        public PersonalityType()
        {
            this.Careers = new HashSet<Career>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string Specifications { get; set; }
    
        public virtual ICollection<Career> Careers { get; set; }
    }
}
