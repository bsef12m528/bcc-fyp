﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Models;

namespace DAL.Classes
{
    public class UserUniversity
    {
        DbEntities db;

        public UserUniversity()
        {
            db = new DbEntities();
            //db.Configuration.ProxyCreationEnabled = false;

            //db.Configuration.LazyLoadingEnabled = true;
        }

        public List<University> GetUniversities()
        {
            return db.Universities.ToList();
        }

        public List<University> GetUniversitiesBySector(string sector)
        {
            return db.Universities.Where(x => x.Sector == sector).ToList();
        }


        //returns list of campuses according to cities
        public List<UniversityCampuss> GetCampusesByCity(List<int>citiesId)
        {
            List<UniversityCampuss> list = new List<UniversityCampuss>();
            foreach(int cityId in citiesId)
            {
                List<UniversityCampuss> temp = db.UniversityCampusses.Where(x => x.City.Id == cityId).ToList();
                foreach(UniversityCampuss uc in temp)
                {
                    list.Add(uc);
                }
            }
            return list;
        }

        public List<UniversityCampuss>GetAllCampuses()
        {
            List<UniversityCampuss> list = db.UniversityCampusses.ToList();
            return list;
        }


        public University GetUniversityDetail(int id)
        {
            if (db.Universities.Any(x => x.Id == id))
            {
                return db.Universities.First(x => x.Id == id);
            }
            else
                return null;
        }

        public UniversityCampuss GetCampusDetail(int id)
        {
            if (db.UniversityCampusses.Any(x => x.Id == id))
                return db.UniversityCampusses.First(x => x.Id == id);
            else
                return null;
        }


        public List<UniversityNewss>GetUniversityNews(int uniId)
        {
            return db.UniversityNewsses.Where(x=>x.UniversityId==uniId).OrderByDescending(x => x.Date).ToList();
        }


        public List<UniversityNewss> GetNews()
        {
            List<UniversityNewss> list = db.UniversityNewsses.OrderByDescending(x => x.Date).ThenBy(y => y.University.Priority).ToList();
            return list;
        }


        //for getting all universites and their details of subscription
        public object GetSubscriptionsWithUniversityDetails(int userId)
        {
            var query = db.Universities.Select(z => new
            {
                z.Id,
                z.Sector,
                z.Name,
                z.Description,
                z.Priority,
                z.AlternateName,
                z.WebAddress,
                 Category = new 
                         {
                             Id =  z.Category.Id,
                             Name = z.Category.Name,
                             Description = z.Category.Description
                         },
                isSubscribed = z.UserSubscriptionToUniversities.Any(x => x.UserId == userId)
            });
            return query;
        }

        
        public object GetSubscribedUniversities(int userId)
        {
            var query = from ustu in db.UserSubscriptionToUniversities
                        join u in db.Universities on ustu.UniversityId equals u.Id
                        where ustu.UserId == userId
                        select new
                        {
                            u.Id,
                            u.Sector,
                            u.Name,
                            u.Description,
                            u.Priority,
                            u.AlternateName,
                            u.WebAddress,
                             Category = new 
                         {
                             Id =  u.Category.Id,
                             Name = u.Category.Name,
                             Description = u.Category.Description
                         },
                            isSubscribed = true
                        };

            return query;
        }

        public bool GetUserSubscription(UserSubscriptionToUniversity ust)
        {
            return db.UserSubscriptionToUniversities.Any(x => x.UniversityId == ust.UniversityId && x.UserId == ust.UserId);
        }

        public bool DeleteUserSubscription(UserSubscriptionToUniversity ust)
        {
            try
            {
                UserSubscriptionToUniversity u = db.UserSubscriptionToUniversities.First(x => x.UniversityId == ust.UniversityId && x.UserId == ust.UserId);
                db.UserSubscriptionToUniversities.Remove(u);
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool AddUserSubscription(UserSubscriptionToUniversity ust)
        {
            try
            {
                db.UserSubscriptionToUniversities.Add(ust);
                db.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<UniversityNewss> UserSubscribedNews(int userId)
        {
            //return db.UniversityNewsses.Where( x=>x.University.UserSubscriptionToUniversities.Where(y=>y.UserId==userId) ).OrderByDescending(x => x.Date).ThenBy(y => y.University.Priority).ToList();
            var query = from us in db.UserSubscriptionToUniversities
                        join u in db.Universities on us.UniversityId equals u.Id
                        join un in db.UniversityNewsses on u.Id equals un.UniversityId
                        where us.UserId == userId
                        select un;

            return query.OrderByDescending(x => x.Date).ThenBy(y => y.University.Priority).ToList();
        
        }

    }
}