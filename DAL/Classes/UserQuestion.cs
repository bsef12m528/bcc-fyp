﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;
using System.Diagnostics;

namespace DAL.Classes
{
    public class UserQuestion
    {
        DbEntities db;

        public UserQuestion()
        {
            db = new DbEntities();
        }

        public List<Question> GetQuestions()
        {
            return db.Questions.ToList();
        }

        public object GetResult(List<AQuestion> attemptedQuestions)
        {

            //Get Questions entities which are sent 
            int[] arr = attemptedQuestions.Select(x => x.QuestionId).ToArray();
            List<Question> originalQuestionList = db.Questions.ToList();
            List<Question> result = originalQuestionList.Where(x => arr.Contains(x.Id)).ToList();   //result will keep the questions that were sent in this test

            //Not a POCO class. It will determine the points for each career
            List<CareerResult> cAns = db.Careers.Select(x => new
            CareerResult
            {
                CareerId = x.Id,
                PersonalityTypeId = x.PersonalityTypeId,
                TotalPoints = 0
            }).ToList();

            foreach (Question q in result)
            {
                foreach (QuestionCareer qc in q.QuestionCareers)
                {
                    int ans = attemptedQuestions.First(x => x.QuestionId == q.Id).AttemptedAnswer;
                    cAns.First(x => x.CareerId == qc.CareerId).TotalPoints += ans;
                }
            }


            //Getting sum for each PersonalityType -- finalResult keeps score for each PersonalityType
            var finalResult = cAns.GroupBy(x => x.PersonalityTypeId).Select(
                t => new
                {
                    Sum = t.Sum(v => v.TotalPoints), t.First().PersonalityTypeId
                }
            ).ToList();


            //The personalityType that has won this contest
            var selectedPersonality = finalResult.Where(z => z.Sum == finalResult.Max(p => p.Sum)).First();
            
            //getting careers in descending order of interest
            var sc = from ca in cAns
                     join c in db.Careers on ca.CareerId equals c.Id
                     where ca.PersonalityTypeId == selectedPersonality.PersonalityTypeId
                     orderby ca.TotalPoints descending
                     select new SelectedCareers
                     {
                         Id =  c.Id,
                         CareerName =  c.CareerName,
                         Details =  c.Details
                     };

            List<SelectedCareers> selectedCareers = sc.ToList();


            
           string shuffledSummary = GeneratePersonalitySummary(selectedPersonality.PersonalityTypeId);

           
            var q2 = from q in attemptedQuestions
                     join qc in db.QuestionCareers on q.QuestionId equals qc.QuestionId
                     join c in db.Careers on qc.CareerId equals c.Id
                     where c.PersonalityTypeId == selectedPersonality.PersonalityTypeId
                     select  q;

            var q3 = q2.Distinct().ToList();
            string interestSummary = GenerateReportFromQuestions(q3);

            var json = new InterestReport()
            {
                InterestSummary = interestSummary,
                PersonalitySummary = shuffledSummary,
                SelectedCareers = selectedCareers
            };


            return json;
        }


        //generates personality Summary
        private string GeneratePersonalitySummary(int? personalityId)
        {
            //It will produce shuffled summary of your matching personalityType
            string[] list = db.PersonalityTypes.First(x => x.Id == personalityId).Specifications.Split('.');
            Random rnd = new Random();
            List<string> MyRandomArray = list.OrderBy(x => rnd.Next()).ToList();
            string ab = "";
            MyRandomArray.Remove(ab);
            string shuffledSummary = "";
            foreach (string str in MyRandomArray)
            {
                shuffledSummary += str + ".";
            }

            return shuffledSummary.Trim();
        }

        //generate ReportSummary
        private string GenerateReportFromQuestions(List<AQuestion> list)
        {
            List<string> newList = new List<string>();

            foreach(AQuestion q in list)
            {
                if(q.AttemptedAnswer==0)
                {
                    q.Statement = q.Statement.Replace("I have", "You have little");
                    q.Statement = q.Statement.Replace("I am", "You are little");
                    q.Statement = q.Statement.Replace("I like", "You sometimes like");
                    q.Statement = q.Statement.Replace("I", "You very less times");
                    q.Statement = q.Statement.Replace("my" , "your");
                    newList.Add(q.Statement);
                }
                else if (q.AttemptedAnswer == 1)
                {
                    string temp = "";
                    q.Statement = q.Statement.Replace("I have", "You have some");
                    q.Statement = q.Statement.Replace("I am", "You are somewhat");
                    q.Statement = q.Statement.Replace("I like", "You often like");
                    q.Statement = q.Statement.Replace("I", "You sometimes");
                    q.Statement = q.Statement.Replace("my", "your");
                    newList.Add(q.Statement);
                }
                else if (q.AttemptedAnswer == 2)
                {
                    string temp = "";
                    q.Statement = q.Statement.Replace("I have", "You have");
                    q.Statement = q.Statement.Replace("I am", "You are");
                    q.Statement = q.Statement.Replace("I like", "You like");
                    q.Statement = q.Statement.Replace("I", "You often");
                    q.Statement = q.Statement.Replace("my", "your");
                    newList.Add(q.Statement);
                }
            }

            Random rnd = new Random();
            List<string> MyRandomArray = newList.OrderBy(x => rnd.Next()).ToList();
            string shuffledSummary = "";
            foreach (string str in MyRandomArray)
            {
                shuffledSummary += " " + str;
            }

            return shuffledSummary.Trim();
        }
    }
}
