﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Classes
{
    public class AdminPersonality
    {
          DbEntities db;

        public AdminPersonality()
        {
            db = new DbEntities();
        }
        public void Add(PersonalityType obj)
        {
            db.PersonalityTypes.Add(obj);
            db.SaveChanges();
        }
            
        public void Delete(int id)
        {
            PersonalityType obj= db.PersonalityTypes.First(x=>x.Id==id);
            db.PersonalityTypes.Remove(obj);
            db.SaveChanges();
        }

        public void Update(PersonalityType obj)
        {
            db.PersonalityTypes.Attach(obj);
            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public PersonalityType GetDetails(int id)
        {
            return db.PersonalityTypes.First(x => x.Id == id);
        }

        public List<PersonalityType> GetAll()
        {
            return db.PersonalityTypes.ToList();
        }
    }
}
