﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Models;
using System.Data.Entity;

namespace DAL.Classes
{
    public class AdminUniversity
    {
        DbEntities db = new DbEntities();

        public int Add(University obj)
        {
            obj=db.Universities.Add(obj);
            db.SaveChanges();
            return obj.Id;
        }

        public void Update(University obj, UniversityCampuss [] arr)
        {
            db.Universities.Attach(obj);
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();

            List<UniversityCampuss> list = db.UniversityCampusses.Where(x => x.UniversityId == obj.Id).ToList();
            if(arr!=null)
            {
                foreach (var a in arr)
                {
                    if (a.Id == 0)
                    {
                        a.UniversityId = obj.Id;
                        db.UniversityCampusses.Add(a);
                        db.SaveChanges();
                    }
                    else
                    {
                        UniversityCampuss tmp = list.First(x => x.Id == a.Id);
                        list.Remove(tmp);
                    }
                }
            }
           
            if(list!=null)
            {
                foreach (var a in list)
                {
                    db.UniversityCampusses.Remove(a);
                    db.SaveChanges();
                }
            }
        }
        
        public void Delete(int id)
        {
            University obj = db.Universities.First(x => x.Id == id);
            db.Universities.Remove(obj);
            db.SaveChanges();
        }

        public University GetDetails(int id)
        {
            University obj = db.Universities.First(x => x.Id == id);
            return obj;
        }

        public List<University> GetAll()
        {
            return db.Universities.ToList();
        }
        public IEnumerable<City> GetCities()
        {
            return db.Cities;
        }

        public IEnumerable<Category> GetCategories()
        {
            return db.Categories;
        }

        public void AddCampuses(int universityid, UniversityCampuss [] arr)
        {
            if(arr!=null)
            {
                foreach (var obj in arr)
                {
                    obj.UniversityId = universityid;
                    db.UniversityCampusses.Add(obj);
                    db.SaveChanges();
                }
            }
        }
    }
}