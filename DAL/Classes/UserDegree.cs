﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Classes
{
    public class UserDegree
    {
        DbEntities db;
        
        public UserDegree()
        {
            db = new DbEntities();
        }
    
        public Degree GetDegreeDetails(int id)
        {
            return db.Degrees.First(X => X.Id == id);
        }

        public List<Degree>GetAll()
        {
            return db.Degrees.ToList();
        }


        //public bool TrimAndRemoveSpace()
        //{
        //    try
        //    {
        //        foreach (Degree dg in db.Degrees)
        //        {
        //            dg.DegreeName = dg.DegreeName.Replace("&nbsp;", " ");
        //            dg.DegreeName = dg.DegreeName.Trim();
        //            db.SaveChanges();
        //        }

        //        return true;
        //    }
        //    catch (Exception exc)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

    }
}
