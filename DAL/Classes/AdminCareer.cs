﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Models;
using System.Data.Entity;

namespace DAL.Classes
{
    public class AdminCareer
    {
        DbEntities db = new DbEntities();

        public int Add(Career obj)
        {
            Career c=db.Careers.Add(obj);
            db.SaveChanges();
            return c.Id;
        }
        public void Delete(int id)
        {
            Career obj = db.Careers.First(x => x.Id == id);
            db.Careers.Remove(obj);
            db.SaveChanges();
        }
        public void Update(Career obj)
        {
            db.Careers.Attach(obj);
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
        }
        public Career GetDetails(int id)
        {
            Career obj = db.Careers.First(x => x.Id == id);
            return obj;
        }
        public List<Career> GetAll()
        {
            return db.Careers.ToList();
        }
        public void AddDegrees(int careerid, int [] arr)
        {
            if(arr!=null)
            {
                foreach (int a in arr)
                {
                    DegreeCareer obj = new DegreeCareer();
                    obj.CareerId = careerid;
                    obj.DegreeId = a;
                    db.DegreeCareers.Add(obj);
                    db.SaveChanges();
                }
            }
            
        }

        public void DeleteCareerDegrees(int careerid)
        {
            try
            {
                while(true)
                {
                    DegreeCareer obj = db.DegreeCareers.First(x => x.CareerId == careerid);
                    db.DegreeCareers.Remove(obj);
                    db.SaveChanges();
                }
            }catch(Exception e) { }
        }

        public IEnumerable<PersonalityType> GetPersonalityTypes()
        {
            return db.PersonalityTypes;
        }
        public IEnumerable<Degree> GetDegrees()
        {
            return db.Degrees;
        }
    }
}