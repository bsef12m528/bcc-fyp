﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Classes
{
   public class AdminQuestion
    {
       DbEntities db = new DbEntities();

       public int Add(Question obj)
       {
           obj=db.Questions.Add(obj);
           db.SaveChanges();
           return obj.Id;
       }
       public void Update(Question obj,int [] aar)
       {
           db.Questions.Attach(obj);
           db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
           db.SaveChanges();
           UpdateCareers(obj, aar);
       }

       public void UpdateCareers(Question obj, int [] aar)
       {
           var ex = db.QuestionCareers.Where(x => x.QuestionId == obj.Id).Select(x => x).ToList();
           if(aar!=null)
           {
               foreach (var a in aar)
               {
                   QuestionCareer tmp = new QuestionCareer();
                   tmp.CareerId = a;
                   tmp.QuestionId = obj.Id;

                   if (ex.Contains(tmp))
                   {
                       ex.Remove(tmp);
                   }
                   else
                   {
                       db.QuestionCareers.Add(tmp);
                       db.SaveChanges();
                   }
               }
           }

           if(ex!=null)
           {
               foreach (var a in ex)
               {
                   db.QuestionCareers.Remove(a);
                   db.SaveChanges();
               }
           }
       }

       public Question GetDetails(int id)
       {
           return db.Questions.First(x => x.Id == id);
       }
       public List<Question> GetAll()
       {
           return db.Questions.ToList();
       }
       public void Delete(int id)
       {
           Question obj = db.Questions.First(x => x.Id == id);
           db.Questions.Remove(obj);
           db.SaveChanges();
       }
       public IEnumerable<Career> GetCareer()
       {
           return db.Careers;
       }
       public void AddQuestionCareer(int qid, int[] arr)
       {
           if(arr!=null)
           {
               foreach (var a in arr)
               {
                   QuestionCareer obj = new QuestionCareer();
                   obj.CareerId = a;
                   obj.QuestionId = qid;
                   db.QuestionCareers.Add(obj);
                   db.SaveChanges();
               }
           }
       }
    }
}
