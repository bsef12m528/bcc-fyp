﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Models;
using System.Data.Entity;

namespace DAL.Classes
{
    public class AdminNewsFeed
    {
        DbEntities db;

        public AdminNewsFeed()
        {
            db = new DbEntities();
        }
        public void Add(UniversityNewss obj)
        {
            db.UniversityNewsses.Add(obj);
            db.SaveChanges();
        }
            
        public void Delete(int id)
        {
            UniversityNewss obj= db.UniversityNewsses.First(x=>x.Id==id);
            db.UniversityNewsses.Remove(obj);
            db.SaveChanges();
        }

        public void Update(UniversityNewss obj)
        {
            db.UniversityNewsses.Attach(obj);
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
        }

        public UniversityNewss GetDetails(int Id)
        {
            UniversityNewss obj=db.UniversityNewsses.First(x => x.Id == Id);
            return obj;
        }

        public List<UniversityNewss> GetAll()
        {
            List<UniversityNewss> list = db.UniversityNewsses.ToList();
            return list;
        }
        public IEnumerable<University> GetUniversities()
        {
            return db.Universities;
        }


        public string AddNews(List<UniversityNewss> list, string alternateName)
        {
            try
            {
                DbEntities db = new DbEntities();
                University u = db.Universities.First(x => x.AlternateName == alternateName);
                foreach(UniversityNewss news in list)
                {
                    news.UniversityId = u.Id;
                    db.UniversityNewsses.Add(news);
                }

                db.SaveChanges();
                
                return "success";
            }
            catch (Exception exc)
            {
                return exc.Message;
            }

        }
    }
}