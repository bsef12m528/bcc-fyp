﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DAL.Classes
{
   public class UserReport
    {
        DbEntities db;
        
        public UserReport()
        {
            db = new DbEntities();
        }
    
        public bool SaveReport(UserTestReport rpt)
        {
            try
            {
                UserTestReport utr = new UserTestReport
                   {
                       Date = DateTime.Now,
                       ReportSummary = rpt.ReportSummary,
                       UserId = rpt.UserId
                   };

                db.UserTestReports.Add(utr);
                db.SaveChanges();

                foreach (RecommendedCareer c in rpt.RecommendedCareers)
                {
                    RecommendedCareer rc = new RecommendedCareer
                    {
                        UserTestReportId = utr.Id,
                        CareerId = c.CareerId
                    };

                    db.RecommendedCareers.Add(rc);
                }

                db.SaveChanges();
                return true;
            }
            catch (Exception exc)
            {
                return false;
            }

            //return db.Degrees.First(X => X.Id == id);
        }

        public UserTestReport GetReport(int id)
        {
            //db.Configuration.LazyLoadingEnabled = false;
            //UserTestReport utr = db.UserTestReports.Include(x => x.ReportSummary).First();


            //return db.UserTestReports.Include(x=>x.RecommendedCareers).Include(y=>y.RecommendedCareers.Select(z=>z.Career)).Single(x=>x.Id==id);

            return db.UserTestReports.First(x => x.Id == id);
        
        }

       public List<UserTestReport>GetUserReport(int userId)
        {
            return db.UserTestReports.Where(x => x.UserId == userId).ToList();
        }

    }
}
