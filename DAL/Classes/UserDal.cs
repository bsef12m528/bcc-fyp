﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Models;

namespace DAL.Classes
{
    public class UserDal
    {
        DbEntities db;

        public UserDal()
        {
            db = new DbEntities();
        }

        public bool Signup(User a)
        {
            if (db.Users.Any(h => h.Email == a.Email))
                return false;

            User u = db.Users.Add(a);
            db.SaveChanges();
            return true;

        }

        public User Signin(Models.User a)
        {
            if (db.Users.Any(h => h.Email == a.Email && h.Password == a.Password))
            {

                User us = db.Users.First(h => h.Email == a.Email && h.Password == a.Password);
                return new User
                {
                    Email = us.Email,
                    Id = us.Id,
                    Name = us.Name,
                    Password = us.Password,
                    Status = us.Status,
                    Username = us.Username
                };
            }

            else
                return null;
        }

        public string UpdateUser(User us)
        {
            try
            {
                User existing = db.Users.First(x => x.Id == us.Id);
                existing.Password = us.Password;
                db.SaveChanges();
                return "success";
            }
            catch(Exception exc)
            {
                return exc.Message;
            }

        }

        public string GetConString()
        {
           return db.Database.Connection.ConnectionString;
        }

    }
}