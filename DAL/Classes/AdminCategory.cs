﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Classes
{
    public class AdminCategory
    {
          DbEntities db;

          public AdminCategory()
        {
            db = new DbEntities();
        }
          public void Add(Category obj)
        {
            db.Categories.Add(obj);
            db.SaveChanges();
        }
            
        public void Delete(int id)
        {
            Category obj = db.Categories.First(x => x.Id == id);
            db.Categories.Remove(obj);
            db.SaveChanges();
        }

        public void Update(Category obj)
        {
            db.Categories.Attach(obj);
            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public Category GetDetails(int id)
        {
            return db.Categories.First(x => x.Id == id);
        }

        public List<Category> GetAll()
        {
            return db.Categories.ToList();
        }
    }
}
