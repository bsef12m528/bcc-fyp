﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Classes
{
    public class AdminAuthentication
    {
          DbEntities db;

        public AdminAuthentication()
        {
            db = new DbEntities();
        }

        public void Add(Administrator obj)
        {
            db.Administrators.Add(obj);
            db.SaveChanges();
        }
            
        public void Delete(int id)
        {
            Administrator obj= db.Administrators.First(x=>x.Id==id);
            db.Administrators.Remove(obj);
            db.SaveChanges();
        }

        public Administrator GetDetails(Administrator obj)
        {
            try
            {
                Administrator tmp = db.Administrators.First(x => x.Username == obj.Username);
                return tmp;
            }catch(Exception e)
            {
                Administrator tmp2 = null;
                return tmp2;
            }
        }

        public void Update(Administrator obj)
        {
            db.Administrators.Attach(obj);
            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public List<Administrator> GetAll()
        {
            return db.Administrators.Where(x=>x.Role=="contributer").Select(x=>x).ToList();
        }
    }
}
