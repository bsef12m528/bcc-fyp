﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Models;
using System.Data.Entity;

namespace DAL.Classes
{
    public class AdminCampus
    {
        DbEntities db = new DbEntities();

        public void Add(UniversityCampuss obj, CampusDegree [] arr)
        {
            obj=db.UniversityCampusses.Add(obj);
            db.SaveChanges();
            if(arr!=null)
            {
                foreach (var a in arr)
                {
                    a.UniversityCampusId = obj.Id;
                    db.CampusDegrees.Add(a);
                    db.SaveChanges();
                }
            }
        }

        public void Update(UniversityCampuss obj, CampusDegree [] arr)
        {
            db.UniversityCampusses.Attach(obj);
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();

            List<CampusDegree> list = db.CampusDegrees.Where(x => x.UniversityCampusId == obj.Id).ToList();
            List<CampusDegree> exists= obj.CampusDegrees.ToList();

            if(arr!=null)
            {
                foreach(CampusDegree a in arr)
                {
                    int count = list.Where(x=>x.Id==a.Id).Count();
                    if(count!=0)
                    {
                        CampusDegree tmp = list.First(x => x.Id == a.Id);
                        tmp.DegreeUrl = a.DegreeUrl;
                        db.CampusDegrees.Attach(tmp);
                        db.Entry(tmp).State = EntityState.Modified;
                        db.SaveChanges();
                        list.Remove(tmp);
                    }
                    else
                    {
                        a.UniversityCampusId = obj.Id;
                        db.CampusDegrees.Add(a);
                        db.SaveChanges();
                    }
                }
            }
            
            if(list!=null)
            {
                foreach (var a in list)
                {
                    db.CampusDegrees.Remove(a);
                    db.SaveChanges();
                }
            }
        }

        public void Delete(int id)
        {
            UniversityCampuss obj = db.UniversityCampusses.First(x => x.Id == id);
            db.UniversityCampusses.Remove(obj);
            db.SaveChanges();
        }
        public List<UniversityCampuss> GetAll()
        {
            return db.UniversityCampusses.ToList();
        }
        public UniversityCampuss GetDetails(int id)
        {
            UniversityCampuss tmp = db.UniversityCampusses.First(x => x.Id == id);
            return tmp;
        }
        public IEnumerable<University> GetUniversities()
        {
            return db.Universities;
        }
        public IEnumerable<City> GetCities()
        {
            return db.Cities;
        }
        public IEnumerable<Degree> GetDegrees()
        {
            return db.Degrees;
        }
    }
}