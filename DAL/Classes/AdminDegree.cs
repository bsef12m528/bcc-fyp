﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.Models;
using System.Data.Entity;

namespace DAL.Classes
{
    public class AdminDegree
    {
        DbEntities db = new DbEntities();

        public int Add(Degree obj)
        {
            Degree tmp= db.Degrees.Add(obj);
            db.SaveChanges();
            return tmp.Id;
        }
        public void AddCareers(int degreeid, int [] arr)
        {
            if(arr!=null)
            {
                foreach (int a in arr)
                {
                    DegreeCareer tmp1 = new DegreeCareer();
                    tmp1.CareerId = a;
                    tmp1.DegreeId = degreeid;
                    db.DegreeCareers.Add(tmp1);
                    db.SaveChanges();
                }
            }
        }
        public void Delete(int id)
        {
            DeleteCareers(id);
            Degree obj = db.Degrees.First(x => x.Id==id);
            db.Degrees.Remove(obj);
            db.SaveChanges();
        }
        public void Update(Degree obj,int [] arr)
        {
            db.Degrees.Attach(obj);
            db.Entry(obj).State = EntityState.Modified;
            db.SaveChanges();
            DeleteCareers(obj.Id);
            AddCareers(obj.Id, arr);
        }
        public Degree GetDetails(int id)
        {
            Degree obj = db.Degrees.First(x => x.Id == id);
            return obj;
        }
        public List<Degree> GetAll()
        {
            return db.Degrees.ToList();
        }
        public IEnumerable<Career> GetCareers()
        {
            return db.Careers;
        }

        //if cascade deletion is disabled the deletion of sub entities will be done in this fashion otherwise
        //there is no need of this function
        public void DeleteCareers(int DegreeId)
        {
            try
            {
                while(true)
                {
                    DegreeCareer obj = db.DegreeCareers.First(x => x.DegreeId == DegreeId);
                    db.DegreeCareers.Remove(obj);
                    db.SaveChanges();
                }
            }catch(Exception e)
            { }
        }
    }
}