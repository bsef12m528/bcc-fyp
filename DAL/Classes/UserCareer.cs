﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Classes
{
    public class UserCareer
    {
         DbEntities db;

        public UserCareer()
        {
            db = new DbEntities();
        }

        public Career GetCareer(int id)
        {
            return db.Careers.First(x => x.Id == id);
        }

    }
}
