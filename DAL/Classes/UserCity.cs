﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Classes
{
    public class UserCity
    {
        DbEntities db = new DbEntities();
        public List<City> GetAllCities()
        {
            return db.Cities.ToList();
        }

    }
}
