﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Classes
{
    public class AdminCity
    {
         DbEntities db;

          public AdminCity()
        {
            db = new DbEntities();
        }
          public void Add(City obj)
        {
            db.Cities.Add(obj);
            db.SaveChanges();
        }
            
        public void Delete(int id)
        {
            City obj = db.Cities.First(x => x.Id == id);
            db.Cities.Remove(obj);
            db.SaveChanges();
        }

        public void Update(City obj)
        {
            db.Cities.Attach(obj);
            db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public City GetDetails(int id)
        {
            return db.Cities.First(x => x.Id == id);
        }

        public List<City> GetAll()
        {
            return db.Cities.ToList();
        }
    }
}
